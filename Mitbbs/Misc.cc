#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;
int main(int argc, char *argv[])
{
  {
    vector<int> data = {1, 0, 0, 3, 2, 5, 0, 6, 0};
    int j = 0;
    for(int i = 0; i < data.size(); ++i) {
      if(data[i] == 0) {
        swap(data[i], data[j]);
        j++;
      }
    }
    for(auto & v : data) cout << v << std::endl;
    std::cout << "------" << std::endl;
  }

  {
    vector<int> data = {1, 0, 0, 3, 2, 5, 0, 6, 0};
    int cnt = data.size();
    int i = 0, j = data.size() - 1;
    while(cnt) {
      if(data[i] == 0) {
        swap(data[i], data[j]);
        j--;
      } else {
        i++;
      }
      cnt --;
    }
    std::cout << i << std::endl;
    for(auto & v : data) cout << v << std::endl;
    std::cout << "------" << std::endl;
  }

  {
    int K = 2;
    //vector<char> data = {'E', 'A', 'B', 'C', 'D'};
    //vector<char> data = {'A', 'A', 'A'};
    //vector<char> data = {'A', 'B', 'A', 'B', 'A', 'B'};
    //vector<char> data = {'A', 'A', 'B', 'A', 'B', 'C', 'D'};
    vector<char> data = {'A', 'A', 'B', 'A', 'B', 'B', 'C', 'D'};
    unordered_map<char, int> dict;
    dict[data[0]] = 0;
    int space = 0;
    for(int i = 1; i < data.size(); ++i) {
      if(dict.count(data[i])) {
        int delta = i + space - dict[data[i]];
        if(delta <= K) {
          space += K + 1 - delta; 
          dict[data[i]] = i + space;
          continue; 
        } 
      } 
      dict[data[i]] = i + space; 
    }
    cout << data.size() + space << endl;
    std::cout << "------" << std::endl;
  }
  return 0;
}
