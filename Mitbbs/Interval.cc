// Given a array of pairs where each pair contains the start and end time of a meeting (as in int),
// Determine if a single person can attend all the meetings
// Input array(pair(1,4), pair(4, 5), pair(3,4), pair(2,3))
// Output: False

// determine the minimum number of meeting rooms needed to hold all the meetings.
// Input array(pair(1, 4), pair(2,3), pair(3,4), pair(4,5))
// Output: 2
// (1,3), (2, 6), (4,5)
// 1 2 3 4 5 6
// 1 2 1 2 1 0   <-- 走的过程中最大值就是解


#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

struct Interval {
  int start;
  int end;
  Interval() : start(0), end(0) {}
  Interval(int s, int e) : start(s), end(e) {}
};

bool foo(vector<Interval> & data) {
  auto cmp_lambda = [] (Interval a, Interval b) {
    if(a.start < b.start) return true;
    if(a.start > b.start) return false;
    return a.end < b.end;
  };
  sort(data.begin(), data.end(), cmp_lambda);
  Interval r(data[0].start, data[0].end);
  int right = data[0].end;
  for(int i = 1; i < data.size(); ++i) {
    if(data[i].start < right) return false;
    else {
      right = data[i].end;
    } 
  }
  return true;
}

// greedy solution
int goo(vector<Interval> & data) {
  vector<pair<int, char> > tmp;
  for(int i = 0; i < data.size(); ++i) {
    tmp.push_back(make_pair(data[i].start, 's'));
    tmp.push_back(make_pair(data[i].end, 'e'));
  } 
  auto cmp_lambda = [] (pair<int, char> a, pair<int, char> b) {
    if(a.first < b.first) return true;
    if(a.first > b.first) return false;
    if(a.second == 's' && b.second == 'e') return false;
    if(a.second == 'e' && b.second == 's') return true;
    return true;
  };
  sort(tmp.begin(), tmp.end(), cmp_lambda);
  int r = 0;
  int cnt = 0;
  for(int i = 0; i < tmp.size(); ++i) {
    r = max(r, cnt);
    if(tmp[i].second == 's') cnt ++;
    if(tmp[i].second == 'e') cnt --;
  }
  return r;
}

int main(int argc, char *argv[])
{
  {
    vector<Interval> data;
    data.push_back(Interval(1, 4));
    data.push_back(Interval(4, 5));
    data.push_back(Interval(3, 4));
    data.push_back(Interval(2, 3));
    cout << foo(data) << endl;
  }
  {
    vector<Interval> data;
    data.push_back(Interval(1, 4));
    data.push_back(Interval(4, 5));
    data.push_back(Interval(6, 7));
    cout << foo(data) << endl;
  }
  {
    vector<Interval> data;
    data.push_back(Interval(1, 4));
    data.push_back(Interval(5, 7));
    data.push_back(Interval(4, 5));
    cout << foo(data) << endl;
  }
  {
    vector<Interval> data;
    data.push_back(Interval(1, 4));
    data.push_back(Interval(3, 4));
    data.push_back(Interval(4, 5));
    cout << foo(data) << endl;
  }
  {
    vector<Interval> data;
    data.push_back(Interval(1, 4));
    data.push_back(Interval(2, 3));
    data.push_back(Interval(3, 4));
    data.push_back(Interval(4, 5));
    cout << goo(data) << endl;
  }
  {
    vector<Interval> data;
    data.push_back(Interval(1, 3));
    data.push_back(Interval(2, 6));
    data.push_back(Interval(4, 5));
    cout << goo(data) << endl;
  }
  return 0;
}
