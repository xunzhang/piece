// http://www.mitbbs.com/article_t/JobHunting/32832027.html
/*
{ "face", "ball", "apple", "art", "ah" }
"htarfbp..."
根据下面的string去给上面list words排序。
就是平常我们按abcd。。。排，这次按string里的letter顺序排
*/

#include <string>
#include <algorithm>
#include <vector>
#include <iostream>
#include <unordered_map>
using namespace std;

void foo(vector<string> & words, string & seq) {
  unordered_map<char, int> D;
  for(size_t i = 0; i < seq.size(); ++i) D[seq[i]] = i;
  auto compare_lambda = [&] (string a, string b) {
    int i = 0, j = 0;
    for(; i < a.size() && j < b.size(); ++i, ++j) {
      if(a[i] == b[i]) continue;
      return D[a[i]] < D[b[j]];
    }
    if(i < a.size()) return false;
    if(j < b.size()) return true;
    return true;
  };
  sort(words.begin(), words.end(), compare_lambda);
}

int main(int argc, char *argv[])
{
  vector<string> words = {"face", "ball", "apple", "art", "ah", "balle"};
  string seq = "ehtfarbpcl";
  foo(words, seq);
  for(auto & w : words) cout << w << endl; // face, ah, art, apple, ball, balle
  return 0;
}
