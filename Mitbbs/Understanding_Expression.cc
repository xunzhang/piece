// http://www.mitbbs.com/article_t/JobHunting/32772147.html
#include <string>
#include <ctype.h>
#include <unordered_map>
using namespace std;

class foo {
piblic:
  foo(char c, int _a) : b(0) {}
  
  foo(int v) : b(v) {}
  
  foo(char c, int _a, int _b) : b(_b) { var[c] = _a; }

  // (x + a + 1) + (2x + b + 1)
  foo& operator+(const foo & obj) {
    for(auto & kv : obj.var) {
      if(var.count(kv.first)) {
        var[kv.first] += kv.second;
      } else {
        var[kv.first] = kv.second;    
      }
    }
    b += obj.b;
    return *this;
  }

  foo& operator+(const int & v) {
    b += v;
    return *this;
  }

  foo& operator*(const int & v) {
    for(auto & kv : var) {
      var[kv.first] *= v;
    }
    b *= v;
  }

  string fmt() {
    string r;
    for(auto & kv : var) {
      r += to_string(var[kv.first]) 
          + to_string(kv.first)
          + "+";
    }
    r += to_string(b);
    return r;
  }

 public:
  unordered_map<char, int> var;
  int b;
};

string parser(string & input) {
  string result;
  foo *exp = NULL;
  for(size_t i = 0; i < input.size(); ++i) {
    string tmp;
    while(i < input.size()) {
      if(input[i] == '(') break;
      if(input[i] == ')') break;
      if(input[i] == '+') break;
      if(input[i] == '-') break;
      if(input[i] == '*') break;
      tmp += input[i];
      i++;
    }
    if(tmp.size() != 0) {
      if(!isalpha(tmp.back())) {
        if(exp == NULL) {
          //
        }
      } else {
        //
      }
    }
  }
}

int main(int argc, char *argv[])
{
  string input1 = "1+b+2";
  string input2 = "(x+1)*3+2*(2*x+5)";
  string input3 = "(x+3)*(x+1)+3+b+2*(2*b+1)";
  string input4 = "1";
  string input5 = "b";
  string input6 = "3*(x+1)";
  cout << parser(input1) << endl;
  cout << parser(input2) << endl;
  cout << parser(input3) << endl;
  cout << parser(input4) << endl;
  cout << parser(input5) << endl;
  cout << parser(input6) << endl;
  return 0;
}
