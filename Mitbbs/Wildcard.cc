// http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=113621&extra=page%3D1%26filter%3Dsortid%26sortid%3D311%26sortid%3D311
// 第三题 通配符， 给定输入字符串 01*0*, *可代表0或1，输出所有可能的结果

#include <iostream>
#include <string>
#include <vector>

using namespace std;

void helper(string & s, string & tmp, vector<string> & result) {
  if(tmp.size() == s.size()) {
    result.push_back(tmp);
    return;
  }
  char c = s[tmp.size()];
  if(c == '*') {
    string ss = tmp + "0";
    helper(s, ss, result);
    string sss = tmp + "1";
    helper(s, sss, result);
  } else {
    string ss = tmp + c;
    helper(s, ss, result);
  }
}

vector<string> wildcard_matching(string s) {
  vector<string> result;
  string tmp;
  helper(s, tmp, result);
  return result;
}

int main(int agrc, char *agrv[])
{
  auto r = wildcard_matching("01*0*");
  for(auto & v : r) {
    cout << v << endl;
  }
  return 0;
}
