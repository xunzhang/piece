// http://www.mitbbs.com/article_t/JobHunting/32838067.html
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <iostream>
using namespace std;
bool foo(vector<int> & num, int target) {
  if(num.size() == 0) return false;
  vector<int> acc(num.size() + 1, 0); // 第一维补0,不然没法判断target = num[0]情况
  acc[0] = 0;
  acc[1] = num[0];
  for(size_t i = 2; i < acc.size(); ++i) {
    acc[i] += acc[i-1] + num[i-1];
  }
  unordered_map<int, int> D;
  for(size_t i = 1; i < acc.size(); ++i) D[acc[i]] = i;
  for(size_t i = 0; i < acc.size(); ++i) {
    if(D.count(acc[i] + target)) {
      if(D[acc[i] + target] != i) { // target = 0情况的处理
        return true;
      }
    }
  }
  return false;
}
int main(int argc, char *argv[])
{
  vector<int> num1 = {1,2,3,4,5,6};
  cout << foo(num1, 10) << endl; // 1
  cout << foo(num1, 1) << endl; // 1
  cout << foo(num1, 5) << endl; // 1
  cout << foo(num1, 2) << endl; // 1
  cout << foo(num1, 0) << endl; // 0
  vector<int> num2 = {1};
  cout << foo(num2, 1) << endl; // 1
  cout << foo(num2, 0) << endl; // 0
  vector<int> num3 = {-1, 4, 10, -2, -3, 7};
  cout << foo(num3, 2) << endl; // 1
  cout << foo(num3, 99) << endl; // 0
  vector<int> num4 = {-2, -1, 1, 3};
  cout << foo(num4, -2) << endl; // 1
  cout << foo(num4, 0) << endl; // 1
  vector<int> num5 = {0};
  cout << foo(num5, 0) << endl; // 1
  vector<int> num6 = {5, -1, -2, 3, 4};
  cout << foo(num6, 0) << endl; // 1
  return 0;
}
