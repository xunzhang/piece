// https://oj.leetcode.com/problems/merge-intervals/

/**
 * Definition for an interval.
 * struct Interval {
 *     int start;
 *     int end;
 *     Interval() : start(0), end(0) {}
 *     Interval(int s, int e) : start(s), end(e) {}
 * };
 */
class Solution {
public:
    vector<Interval> merge(vector<Interval> &intervals) {
        vector<Interval> r;
        if(intervals.size() == 0) { return r; }
        
        auto cmp_lambda = [] (Interval a, Interval b) {
            if(a.start < b.start) return true;
            if(a.start > b.start) return false;
            return a.end < b.end;
        };
        sort(intervals.begin(), intervals.end(), cmp_lambda);
        
        r.push_back(intervals[0]);
        for(int i = 1; i < intervals.size(); ++i) {
            int left = r.back().start, right = r.back().end;
            if(intervals[i].start <= right) {
                r.back().end = max(right, intervals[i].end);
            } else {
                r.push_back(intervals[i]);
            }
        }
        return r;
    }
};