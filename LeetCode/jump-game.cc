// https://oj.leetcode.com/problems/jump-game/

class Solution {
public:
    bool canJump(int A[], int n) {
        int reach = 0;
        for(int i = 0; i <= reach && i < n; ++i) {
            reach = max(reach, A[i] + i);
        }
        if(reach < n - 1) return false;
        return true;
    }
};