// https://oj.leetcode.com/problems/symmetric-tree/

/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    
    bool same(TreeNode *a, TreeNode *b) {
        if(a == NULL && b == NULL) return true;
        if(a == NULL || b == NULL) return false;
        return a->val == b->val && same(a->left, b->right) && same(a->right, b->left);
    }
    
    bool isSymmetric(TreeNode *root) {
        return same(root, root);
    }
    
};