// https://oj.leetcode.com/problems/remove-duplicates-from-sorted-list-ii/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    
    ListNode *deleteDuplicates(ListNode *head) {
        if(!head) return NULL;
        ListNode *pre = NULL, *cur = head;
        ListNode *newhead = NULL;
        while(cur) {
            ListNode *nxt = cur->next;
            while(nxt && nxt->val == cur->val) {
                ListNode *tmp = nxt->next;
                delete nxt;
                nxt = tmp;
            }
            if(pre != NULL) {
                if(nxt == cur->next) { pre->next = cur; pre = pre->next; }
            } else {
                if(nxt == cur->next) { pre = cur; newhead = pre; }
            }
            cur = nxt;
        }
        if(pre) pre->next = cur;
        return newhead;
    }
};