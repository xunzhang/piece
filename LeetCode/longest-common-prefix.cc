// https://oj.leetcode.com/problems/longest-common-prefix/

class Solution {
public:
    
    string longestCommonPrefix(vector<string> &strs) {
        string prefix;
        if(strs.size() == 0) return prefix;
        prefix = strs[0];
        for(int i = 1; i < strs.size(); ++i) {
            int j = 0;
            while(prefix[j] == strs[i][j] && j < strs[i].size() && j < prefix.size()) {
                j++;
            }
            prefix = prefix.substr(0, j);
        }
        return prefix;
    }
    
};