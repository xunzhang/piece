// https://oj.leetcode.com/problems/divide-two-integers/

class Solution {
public:
    
    long long foo(long long a, long long b) {
        if(a < b) return 0;
        long long r = 0, original_b = b;
        long long cnt = 0;
        while(a >= b) {
            r += 1 << cnt;
            cnt += 1;
            a -= b;
            b *= 2;
        }
        if(a == 0) return r;
        return r + foo(a, original_b);
    }
    
    int divide(int dividend, int divisor) {
        if(dividend == 0) return 0;
        bool negative = false;
        if((dividend < 0 && divisor > 0) || (dividend > 0 && divisor < 0)) negative = true;
        long long a = abs(static_cast<long long>(dividend)), b = abs(static_cast<long long>(divisor));
        long long r = foo(a, b);
        if(r > INT_MAX) return negative ? INT_MIN : INT_MAX;
        return negative ? static_cast<int>(-r) : static_cast<int>(r);
    }
    
};