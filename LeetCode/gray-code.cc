// https://oj.leetcode.com/problems/gray-code/

class Solution {
public:
    
    vector<int> grayCode(int n) {
        vector<int> result = {0};
        for(int bit = 0; bit < n; ++bit) {
            int v = 1 << bit;
            for(int k = result.size() - 1; k >= 0; --k) {
                result.push_back(result[k] | v);
            }
        }
        return result;
    }

};