// https://oj.leetcode.com/problems/partition-list/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:

    ListNode *partition(ListNode *head, int x) {
        if(!head) return NULL;
        ListNode *p1 = NULL, *p2 = NULL;
        ListNode *head_p1 = NULL, *head_p2 = NULL;
        while(head) {
            if(head->val < x) {
                if(!p1) { p1 = head; head_p1 = p1; }
                else { p1->next = head; p1 = p1->next; }
            } else {
                if(!p2) { p2 = head; head_p2 = p2; }
                else { p2->next = head; p2 = p2->next; }
            }
            head = head->next;
        }
        if(head_p2) p2->next = NULL;
        if(head_p1) {
            p1->next = head_p2;
        } else {
            return head_p2;
        }
        return head_p1;
    }
    
};