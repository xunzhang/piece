// https://oj.leetcode.com/problems/permutations/

class Solution {
public:

/*
    void nextPermutation(vector<int> &num) {
        int i = num.size() - 2;
        while(i >= 0) {
            if(num[i] < num[i+1]) { break; }
            i --;
        }
        if(i == -1) {
            reverse(num.begin(), num.end());
            return;
        }
        for(int k = num.size() - 1; k > i; --k) {
            if(num[k] > num[i]) {
                swap(num[k], num[i]);
                break;
            }
        }
        reverse(num.begin() + i + 1, num.end());
    }
    
    vector<vector<int> > permute(vector<int> &num) {
        int sz = 1;
        for(int i = 1; i <= num.size(); ++i) { sz *= i; }
        vector<vector<int> > r;
        r.push_back(num);
        for(int i = 1; i < sz; ++i) {
            nextPermutation(num);
            r.push_back(num);
        }
        return r;
    }
*/
    void helper(vector<int> & num, vector<int> & visited, vector<int> & tmp, vector<vector<int> > & result) {
        if(tmp.size() == num.size()) {
            result.push_back(tmp);
            return;
        }
        for(int i = 0; i < num.size(); ++i) {
            if(visited[i] == 0) {
                visited[i] = 1;
                tmp.push_back(num[i]);
                helper(num, visited, tmp, result);
                tmp.pop_back();
                visited[i] = 0;
            }
        }
    }
    
    vector<vector<int> > permute(vector<int> &num) {
        vector<vector<int> > r;
        vector<int> tmp;
        vector<int> visited(num.size(), 0);
        sort(num.begin(), num.end());
        helper(num, visited, tmp, r);
        return r;
    }
};