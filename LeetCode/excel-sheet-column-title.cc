// https://oj.leetcode.com/problems/excel-sheet-column-title/

class Solution {
public:
    string convertToTitle(int n) {
        string r;
        while(n) {
            int v = n % 26 == 0 ? 26 : n % 26;
            r.push_back(v + 'A' - 1);
            n = (n - 1) / 26;
        }
        reverse(r.begin(), r.end());
        return r;
    }
};