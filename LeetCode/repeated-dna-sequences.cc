// https://oj.leetcode.com/problems/repeated-dna-sequences/

class Solution {
public:
    
    Solution() {
        D['A'] = 0;
        D['C'] = 1;
        D['G'] = 2;
        D['T'] = 3;
    }

    int foo(string & s) {
        int r = 0;
        for(int i = 0; i < 10; ++i) r += D[s[i]] * pow(4, i);
        return r;
    }
    
    vector<string> findRepeatedDnaSequences(string s) {
        int K = 10;
        unordered_map<int, int> tmp; vector<string> result;
        if(s.size() < K) return result;
        for(int i = 0; i <= s.size() - K; ++i) {
            string ss = s.substr(i, K);
            int r = foo(ss);
            if(tmp.count(r)) {
                if(tmp[r] == 1) result.push_back(ss);
            }
            tmp[r] += 1;
        }
        return result;
    }
    
private:
    unordered_map<char, int> D;    
};