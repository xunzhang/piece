// https://oj.leetcode.com/problems/flatten-binary-tree-to-linked-list/

/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    
    void flatten(TreeNode *root) {
        if(!root) return;
        TreeNode *p = root->left;
        if(p) {
            while(p->right) p = p->right;
            p->right = root->right;
        }
        flatten(root->left);
        flatten(root->right);
        if(root->left) root->right = root->left;
        root->left = NULL;        
    }
};