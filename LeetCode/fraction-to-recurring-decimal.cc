// https://oj.leetcode.com/problems/fraction-to-recurring-decimal/

class Solution {
public:
    
    string fractionToDecimal(int numerator, int denominator) {
        if(numerator == 0) return "0";
        string result;
        long long a = static_cast<long long>(numerator), b = static_cast<long long>(denominator);
        if(a * b < 0) result += "-";
        a = abs(a); b = abs(b);
        result += to_string(a/b);
        long long r = a % b;
        if(r == 0) return result;
        result += ".";
        unordered_map<int, int> D;
        while(r) {
            if(D.count(r)) {
                result.insert(D[r], 1, '(');
                result += ")";
                break;
            }
            D[r] = result.size();
            result += to_string( (10 * r) / b);
            r = (10 * r) % b;
        }
        return result;
    }

};