// https://oj.leetcode.com/problems/decode-ways/

class Solution {
public:
    
    /*
     * num[1...i] : s[i] = '0', s[i-1] != '1'且'2' -> invalid
     *            : s[i] = '0', s[i-1] = '1'或'2' -> num[i-2]
     *            : s[i] != '0', 如果s[i]能与s[i-1]混合 -> num[i-1] + num[i-2]
     *            : s[i] != '0', 如果s[i]不能与s[i-1]混合 -> num[i-1]
     */
     
    /*
    int numDecodings(string s) {
        if(s.size() == 0) { return 0; }
        if(s[0] == '0') { return 0; }
        vector<int> num(s.size()+1);
        num[0] = 1;
        num[1] = 1;
        for(int i = 2; i <= s.size(); ++i) {
            if(s[i-1] > '9' || s[i-1] < '0') { return 0; }
            if(s[i-1] == '0') {
                if(s[i-2] == '1' || s[i-2] == '2') {
                    num[i] = num[i-2];
                } else {
                    return 0;
                }
            } else {
                if(s[i-2] == '1' || (s[i-2] == '2' && s[i-1] >= '0' && s[i-1] <= '6')) {
                    num[i] = num[i-2] + num[i-1];
                } else {
                    num[i] = num[i-1];
                }
            } // if-else
        } // for
        return num[s.size()];
    }
    */
    
    int numDecodings(string s) {
        if(s.size() == 0) return 0;
        if(s[0] == '0') return 0;
        vector<int> T(s.size() + 1);
        T[0] = 1; T[1] = 1;
        for(int i = 1; i < s.size(); ++i) {
            if(s[i] == '0') {
                if(s[i-1] == '1' || s[i-1] == '2') {
                    T[i+1] = T[i-1];
                } else {
                    T[i+1] = 0;
                }
            } else {
                if((s[i] >= '1' && s[i] <= '6' && s[i-1] == '2') || (s[i] >= '1' && s[i] <= '9' && s[i-1] == '1')) {
                    T[i+1] = T[i] + T[i-1];
                } else {
                    T[i+1] = T[i];
                }
            }
        }
        return T.back();
    }
    
};