// https://oj.leetcode.com/problems/best-time-to-buy-and-sell-stock-iii/

class Solution {
public:
    
    int maxProfit(vector<int> &prices) {
        if(prices.size() == 0) return 0;
        int mmin = prices[0], mmax = prices.back();
        vector<int> start_max(prices.size(), 0), end_max(prices.size(), 0);
        for(int i = 1; i < prices.size(); ++i) {
            mmin = min(mmin, prices[i]);
            end_max[i] = max(end_max[i-1], prices[i] - mmin);
        }
        for(int i = prices.size() - 2; i >= 0; --i) {
            mmax = max(mmax, prices[i]);
            start_max[i] = max(start_max[i+1], mmax - prices[i]);
        }
        int max_profit = 0;
        for(int i = 0; i < prices.size(); ++i) {
            max_profit = max(max_profit, end_max[i] + start_max[i]);
        }
        return max_profit;
    }
};