https://oj.leetcode.com/problems/jump-game-ii/

class Solution {
public:
    int jump(int A[], int n) {
        int reach = 0;
        int step = 0;
        int explore = 0;
        for(int i = 0; i <= reach && i < n; ++i) {
            if(i > explore) {
                explore = reach;
                step ++;
            }
            reach = max(reach, A[i] + i);
        }
        if(reach < n - 1) return 0;
        return step;
    }
};