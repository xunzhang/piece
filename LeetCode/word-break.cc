// https://oj.leetcode.com/problems/word-break/

class Solution {
public:

    /*
    bool wordBreak(string s, unordered_set<string> &dict) {
        for(int i = 0; i < s.size(); ++i) {
            if(dict.count(s.substr(0, i+1)) && wordBreak(s.substr(i + 1, s.size() - i - 1), dict)) return true;
        }
        return false;
    }
    */
    
    bool wordBreak(string s, unordered_set<string> &dict) {
        vector<int> T(s.size());
        T[0] = dict.count(s.substr(0, 1)) ? 1 : 0;
        for(int i = 1; i < s.size(); ++i) {
            if(dict.count(s.substr(0, i + 1))) {
                T[i] = 1;
                continue;
            }
            for(int k = 0; k < i; ++k) {
                if(T[k] && dict.count(s.substr(k + 1, i - k))) {
                    T[i] = 1;
                    break;
                } 
            }
        }
        return T.back();
    }
};