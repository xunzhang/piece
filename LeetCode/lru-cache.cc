// https://oj.leetcode.com/problems/lru-cache/

struct Node {
    int key;
    int value;
    Node *l, *r;
    Node(int k, int v) : key(k), value(v), l(NULL), r(NULL) {}
};

class LRUCache{
public:
    LRUCache(int capacity) {
        sz = capacity;
        head = new Node(-1, -1);
        tail = new Node(-1, -1);
        head->r = tail;
        tail->l = head;
    }
    
    ~LRUCache() {
        Node *prev = NULL;
        while(head) {
            if(prev) delete prev;
            prev = head;
            head = head->r;
        }
    }
    
    int get(int key) {
        if(hashmap.count(key)) {
            int r = hashmap[key]->value;
            detach(hashmap[key]);
            attach(hashmap[key]);
            return r;
        }
        return -1;
    }
    
    void set(int key, int value) {
        if(hashmap.count(key)) {
            detach(hashmap[key]);
            attach(hashmap[key]);
            hashmap[key]->value = value;
        } else {
            Node *new_node = new Node(key, value);
            // do not exist
            if(hashmap.size() < sz) {
                attach(new_node);
            } else {
                Node* tmp = tail->l;
                detach(tmp);
                hashmap.erase(tmp->key);
                delete tmp;
                attach(new_node);
            }
            hashmap[key] = new_node;
        }
    }
    
    void detach(Node* node) {
        node->r->l = node->l;
        node->l->r = node->r;
    }
    
    void attach(Node* node) {
        node->r = head->r;
        node->l = head;
        head->r->l = node;
        head->r = node;
    }
    
private:
    int sz;
    Node *head, *tail;
    unordered_map<int, Node*> hashmap;
};