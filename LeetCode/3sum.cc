// https://oj.leetcode.com/problems/3sum/

class Solution {
public:
    vector<vector<int> > threeSum(vector<int> &num) {
        vector<vector<int> > result;
        if(num.size() <= 2) return result;
        sort(num.begin(), num.end());
        for(int i = 0; i < num.size() - 2 && num[i] <= 0; ++i) {
            if(i != 0 && num[i] == num[i-1]) continue;
            int p1 = i + 1, p2 = num.size() - 1;
            while(p1 < p2) {
                int sum = num[p1] + num[p2] + num[i];
                if(sum == 0) {
                    result.push_back(vector<int>({num[i], num[p1], num[p2]}));
                    p1 += 1; p2 -= 1;
                    while(p1 < p2 && num[p1] == num[p1-1]) p1 += 1;
                    while(p1 < p2 && num[p2] == num[p2+1]) p2 -= 1;
                } else if(sum < 0) {
                    p1 += 1;
                } else {
                    p2 -= 1;
                }
            }
        }
        return result;
    }
};