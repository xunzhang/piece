// https://oj.leetcode.com/problems/recover-binary-search-tree/

/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
 
// http://yucoding.blogspot.com/2013/03/leetcode-question-75-recover-binary.html
class Solution {
public:
    void recoverTree(TreeNode *root) {
        first = NULL, second = NULL, prev = NULL;
        inorder_traverse(root);
        int tmp = second->val;
        second->val = first->val;
        first->val = tmp;
    }
    
    void inorder_traverse(TreeNode *root) {
        if(!root) return;
        inorder_traverse(root->left);
        if(!prev) prev = root;
        if(prev->val > root->val) {
            if(!first) {
                first = prev;
            }
                second = root;
        }
        prev = root;
        inorder_traverse(root->right);
    }
    
private:
    TreeNode *first, *second;
    TreeNode *prev;
};