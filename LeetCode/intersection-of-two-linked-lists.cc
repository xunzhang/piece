// https://oj.leetcode.com/problems/intersection-of-two-linked-lists/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
        int l1 = 0, l2 = 0;
        ListNode *h1 = headA, *h2 = headB;
        ListNode *th1, *th2;
        while(h1) {
            l1 ++;
            if(h1->next == NULL) { th1 = h1; }
            h1 = h1->next;
        }
        while(h2) {
            l2 ++;
            if(h2->next == NULL) { th2 = h2; }
            h2 = h2->next;
        }
        
        if(th1 != th2) { return NULL; }
        
        h1 = headA; h2 = headB;
        if(l1 < l2) {
            int delta = l2 - l1;
            while(delta) { delta --; h2 = h2->next; }
        } else {
            int delta = l1 - l2;
            while(delta) { delta --; h1 = h1->next; }
        }
        
        while(h1 && h2) {
            if(h1 == h2) { return h1; }
            h1 = h1->next; h2 = h2->next;
        }
    }
};