// https://oj.leetcode.com/problems/minimum-window-substring/

class Solution {
public:

    string minWindow(string S, string T) {
        if(S.size() == 0 || T.size() == 0) return "";
        unordered_map<char, int> dict;
        for(int i = 0; i < T.size(); ++i) dict[T[i]] += 1;
        int right = 0, left = 0, minlen = INT_MAX, minstart = 0, cnt = 0;
        for(; right < S.size(); ++right) {
            if(dict.count(S[right])) {
                if(dict[S[right]] > 0) cnt += 1;
                dict[S[right]] -= 1;
            }
            while(cnt == T.size()) {
                if(right - left + 1 < minlen) {
                    minlen = right - left + 1;
                    minstart = left;
                }
                if(dict.count(S[left])) {
                    dict[S[left]] += 1;
                    if(dict[S[left]] > 0) cnt -= 1;
                }
                left += 1;
            }
        }
        if(minlen > S.size()) return "";
        return S.substr(minstart, minlen);
    }

};