// https://oj.leetcode.com/problems/insert-interval/

/**
 * Definition for an interval.
 * struct Interval {
 *     int start;
 *     int end;
 *     Interval() : start(0), end(0) {}
 *     Interval(int s, int e) : start(s), end(e) {}
 * };
 */
class Solution {
public:

    vector<Interval> insert(vector<Interval> &intervals, Interval newInterval) {
        vector<Interval> r;
        if(intervals.size() == 0) { r.push_back(newInterval); return r; }
        
        int i = 0;
        for(; i < intervals.size(); ++i) {
            if(intervals[i].end < newInterval.start) {
                r.push_back(intervals[i]);
            } else {
                break;
            }
        }
        
        if(i == intervals.size()) { r.push_back(newInterval); return r; }
        int left, right;
        left = min(newInterval.start, intervals[i].start);
        
        for(; i < intervals.size(); ++i) {
            if(newInterval.end < intervals[i].end) { break; }
        }
        
        if(i == intervals.size()) { 
            right = newInterval.end; 
            Interval tmp(left, right); r.push_back(tmp);
            return r;
        }
        if(newInterval.end < intervals[i].start) {
            right = newInterval.end;
        } else {
            right = intervals[i].end;
            i += 1;
        }
        Interval tmp(left, right); r.push_back(tmp);
        r.insert(r.end(), intervals.begin() + i, intervals.end());
        return r;
    }
    
};