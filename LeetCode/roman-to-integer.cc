// https://oj.leetcode.com/problems/roman-to-integer/

class Solution {
public:
    Solution() {
        dict['I'] = 1;
        dict['V'] = 5;
        dict['X'] = 10;
        dict['L'] = 50;
        dict['C'] = 100;
        dict['D'] = 500;
        dict['M'] = 1000;
    }
    int romanToInt(string s) {
        int sum = 0;
        if(s.size() == 0) return 0;
        if(s.size() == 1) return dict[s[0]];
        int i = 0;
        for(; i < s.size() - 1;) {
            int v1 = dict[s[i]], v2 = dict[s[i+1]];
            if(v1 >= v2) {
                sum += v1;
                i++;
            } else {
                sum += v2 - v1;
                i+=2;
            }
        }
        if(i < s.size()) {
        //if(dict[s[s.size()-2]] >= dict[s[s.size()-1]]) { 
            sum += dict[s.back()]; 
        }
        return sum;
    }
private:
    unordered_map<char, int> dict;
};