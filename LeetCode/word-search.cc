// https://oj.leetcode.com/problems/word-search/

class Solution {
public:
  
    bool exist(vector<vector<char> > &board, string word) {
        for(int i = 0; i < board.size(); ++i) {
            for(int j = 0; j < board[0].size(); ++j) {
                if(board[i][j] == word[0]) {
                    vector<int> tmp(board[0].size(), 0);
                    vector<vector<int> > visited(board.size(), tmp);
                    if(find(board, word, i, j, visited, 0)) return true;
                }
            }
        }
        return false;
    }
    
    bool find(vector<vector<char> > & board, string & word, int i, int j, vector<vector<int> > & visited, int word_indx) {
         if(i < 0 || j < 0 || i >= board.size() || j >= board[0].size()) return false;
         if(visited[i][j]) return false;
         if(board[i][j] != word[word_indx]) return false;
         if(word_indx == word.size() - 1) return true;
         visited[i][j] = 1;
         bool r =  find(board, word, i, j - 1, visited, word_indx + 1)
            || find(board, word, i, j + 1, visited, word_indx + 1)
            || find(board, word, i - 1, j, visited, word_indx + 1)
            || find(board, word, i + 1, j, visited, word_indx + 1);
         visited[i][j] = 0;
         return r;
    }
    
};