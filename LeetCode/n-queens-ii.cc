// https://oj.leetcode.com/problems/n-queens-ii/

class Solution {
public:

    bool valid(int row, vector<int> & column) {
        for(int i = 0; i < row; ++i) {
            if( (column[row] == column[i]) || ((row - i) == abs(column[row] - column[i])) ) return false;
        }
        return true;
    }

    void helper(int n, int row, vector<int> & column, int & cnt) {
        if(row == n) {
            cnt += 1;
            return;
        }
        for(int i = 0; i < n; ++i) {
            column[row] = i;
            if(valid(row, column)) {
                helper(n, row + 1, column, cnt);
            }
            column[row] = -1;
        }
    }

    int totalNQueens(int n) {
        vector<int> column(n, -1);
        int cnt = 0;
        helper(n, 0, column, cnt);
        return cnt;
    }
};