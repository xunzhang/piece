// https://oj.leetcode.com/problems/3sum-closest/

class Solution {
public:
    int threeSumClosest(vector<int> &num, int target) {
        if(num.size() < 3) return INT_MAX;
        sort(num.begin(), num.end());
        int result = num[0] + num[1] + num[2];
        for(int i = 0; i < num.size() - 2; ++i) {
            int p1 = i + 1, p2 = num.size() - 1;
            while(p1 < p2) {
                int sum = num[i] + num[p1] + num[p2];
                if(abs(sum - target) < abs(result - target)) {
                    result = sum;
                }
                if(sum == target) {
                    return target;
                } else if(sum < target) {
                    p1 += 1;
                } else {
                    p2 -= 1;
                }
            }
        }
        return result;
    }
};