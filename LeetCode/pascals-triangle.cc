// https://oj.leetcode.com/problems/pascals-triangle/

class Solution {
public:
    
    vector<vector<int> > generate(int numRows) {
        vector<vector<int> > rows;
        if(numRows == 0) return rows;
        vector<int> first = {1}; rows.push_back(first);
        if(numRows == 1) return rows;
        vector<int> second = {1, 1}; rows.push_back(second);        
        for(int i = 2; i < numRows; ++i) {
            vector<int> row = {1};
            for(int k = 1; k <= i - 1; ++k) {
                row.push_back(rows[i-1][k-1] + rows[i-1][k]);
            }
            row.push_back(1);
            rows.push_back(row);
        }
        return rows;
    }
};