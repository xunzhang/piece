// https://oj.leetcode.com/problems/median-of-two-sorted-arrays/

class Solution {
public:

    // 注意这里是k-th的数, 因此这里需要 (m + n) / 2 + 1 判断
    double foo(int A[], int m, int B[], int n, int k) {
        if(m == 0) { return B[k-1]; }
        if(n == 0) { return A[k-1]; }
        if(A[m/2] < B[n/2]) {
            if(m/2 + n/2 + 1 >= k) {
                return foo(A, m, B, n / 2, k);
            } else {
                return foo(A + m / 2 + 1, m - (m / 2 + 1), B, n, k - (m / 2 + 1));
            }
        } else {
            if(m/2 + n/2 + 1 >= k) {
                return foo(A, m / 2, B, n, k);
            } else {
                return foo(A, m, B + n / 2 + 1, n - (n / 2 + 1), k - (n / 2 + 1));
            }
        }
    }

    double findMedianSortedArrays(int A[], int m, int B[], int n) {
        return (m + n) % 2 == 0 ? (foo(A, m, B, n, (m + n) / 2) + foo(A, m, B, n, (m + n) / 2 + 1)) / 2: foo(A, m, B, n, (m + n) / 2 + 1);
    }
};