// https://oj.leetcode.com/problems/read-n-characters-given-read4-ii-call-multiple-times/

// Forward declaration of the read4 API.
int read4(char *buf);

class Solution {
public:
    /**
     * @param buf Destination buffer
     * @param n   Maximum number of characters to read
     * @return    The number of characters read
     */
    int read(char *buf, int n) {
        int read_bytes = 0;
        bool eof = false;
        while(!eof && read_bytes < n) {
            if(buffer_sz == 0) {
                buffer_sz = read4(buffer);
                if(buffer_sz < 4) eof = true;
            }
            int sz = min(n - read_bytes, buffer_sz);
            memcpy(buf + read_bytes, buffer + buffer_offset, sz);
            read_bytes += sz;
            buffer_sz -= sz;
            buffer_offset = (buffer_offset + sz) % 4;
        }
        return read_bytes;
    }

private:
    char buffer[4];
    int buffer_sz = 0;
    int buffer_offset = 0;
};