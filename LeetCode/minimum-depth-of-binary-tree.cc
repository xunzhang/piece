// https://oj.leetcode.com/problems/minimum-depth-of-binary-tree/

/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
//     1
//    /
//   2
//   \
//    3
class Solution {
public:
    int minDepth(TreeNode *root) {
        if(!root) return 0;
        if(!root->left && !root->right) return 1;
        int dl = minDepth(root->left);
        int dr = minDepth(root->right);
        if(!root->left) return dr + 1;
        if(!root->right) return dl + 1;
        return min(dl, dr) + 1;
    }
};