// https://oj.leetcode.com/problems/longest-substring-without-repeating-characters/

class Solution {
public:
    // O(n)
    int lengthOfLongestSubstring(string s) {
        if(s == "") return 0;
        int maxlen = INT_MIN, start = 0;
        vector<int> indx_map(256, -1);
        for(int i = 0; i < s.size(); ++i) {
            if(indx_map[s[i]] != -1) {
                for(int j = start; j < indx_map[s[i]]; ++j) {
                    indx_map[s[j]] = -1;
                }
                start = indx_map[s[i]] + 1;
            }
            indx_map[s[i]] = i;
            maxlen = max(maxlen, i - start + 1);
        }
        return maxlen;
    }
};