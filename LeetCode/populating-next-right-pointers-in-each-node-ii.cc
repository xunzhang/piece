// https://oj.leetcode.com/problems/populating-next-right-pointers-in-each-node-ii/

/**
 * Definition for binary tree with next pointer.
 * struct TreeLinkNode {
 *  int val;
 *  TreeLinkNode *left, *right, *next;
 *  TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
 * };
 */
class Solution {
public:
    void connect(TreeLinkNode *root) {
        if(root == NULL) return;
        TreeLinkNode *p = root;
        TreeLinkNode *tmp = NULL;
        // 找tmp
        while(p->next) {
            p = p->next;
            if(p->left) { tmp = p->left; break; }
            if(p->right) { tmp = p->right; break; }
        } // while
        p = tmp;
        if(root->left) root->left->next = root->right == NULL ? p : root->right;
        if(root->right) root->right->next = p;
        connect(root->right); // 注意这先调用右孩子, 因为要用next找到tmp(p)
        connect(root->left);
    }
};