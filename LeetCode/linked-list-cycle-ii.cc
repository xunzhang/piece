// https://oj.leetcode.com/problems/linked-list-cycle-ii/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *detectCycle(ListNode *head) {
        ListNode *it1 = head, *it2 = head;
        bool flag = false;
        while(it2 && it2->next) {
            it1 = it1->next;
            it2 = it2->next->next;
            if(it1 == it2) { flag = true; break; }
        }
        if(flag == false) {
            return NULL; 
        } else {
            it1 = head;
            while(it1 != it2) { it1 = it1->next; it2 = it2->next; }
        }
        return it1;
    }
};