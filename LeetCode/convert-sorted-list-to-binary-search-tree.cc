// https://oj.leetcode.com/problems/convert-sorted-list-to-binary-search-tree/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:

    /*
    // 破坏list, O(n)
    TreeNode *sortedListToBST(ListNode *head) {
        if(head == NULL) return NULL;
        ListNode *l1 = head, *l2 = head, *prev = NULL;
        while(l2->next && l2->next->next) {
            prev = l1;
            l1 = l1->next; l2 = l2->next->next;
        }
        TreeNode *x = new TreeNode(l1->val);
        ListNode *rr = l1->next;
        delete l1;
        if(prev) {
            prev->next = NULL;
            x->left = sortedListToBST(head);
        }
        x->right = sortedListToBST(rr);
        return x;
    }
    */

    // 不破坏list, O(n)，用下标控制
    TreeNode *sortedListToBST(ListNode *head) {
        if(!head) return NULL;
        bak = head;
        int len = 0; ListNode *p = head;
        while(p) { len += 1; p = p->next; }
        return sortedListToBST(0, len - 1);
    }
    
    TreeNode *sortedListToBST(int l, int r) {
        if(l > r) return NULL;
        int mid = l + (r - l) / 2;
        TreeNode *lnode = sortedListToBST(l, mid - 1);
        TreeNode *node = new TreeNode(bak->val);
        node->left = lnode;
        bak = bak->next;
        node->right = sortedListToBST(mid + 1, r);
        return node;
    }
    
private:
    ListNode *bak;
};