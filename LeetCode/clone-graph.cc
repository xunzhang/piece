// https://oj.leetcode.com/problems/clone-graph/

/**
 * Definition for undirected graph.
 * struct UndirectedGraphNode {
 *     int label;
 *     vector<UndirectedGraphNode *> neighbors;
 *     UndirectedGraphNode(int x) : label(x) {};
 * };
 */
class Solution {
public:
    
    UndirectedGraphNode *dfs(UndirectedGraphNode *node) {
        UndirectedGraphNode *new_node = new UndirectedGraphNode(node->label);
        D[node] = new_node;
        for(auto & neighbor : node->neighbors) {
            if(!D.count(neighbor)) {
                D[neighbor] = dfs(neighbor);
            }
            new_node->neighbors.push_back(D[neighbor]);
        }
        return new_node;
    }
    
    UndirectedGraphNode *bfs(UndirectedGraphNode *node) {
        unordered_map<UndirectedGraphNode*, UndirectedGraphNode*> DD;
        queue<UndirectedGraphNode *> q;
        UndirectedGraphNode *new_node = new UndirectedGraphNode(node->label);
        q.push(node);
        DD[node] = new_node;
        while(!q.empty()) {
            UndirectedGraphNode *tmp = q.front(); q.pop();
            for(auto & neighbor : tmp->neighbors) {
                if(!DD.count(neighbor)) {
                    UndirectedGraphNode *nnew_node = new UndirectedGraphNode(neighbor->label);
                    DD[neighbor] = nnew_node;
                    q.push(neighbor);
                }
                DD[tmp]->neighbors.push_back(DD[neighbor]);
            }
        }
        return new_node;
    }
    
    UndirectedGraphNode *cloneGraph(UndirectedGraphNode *node) {
        if(!node) return NULL;
        return dfs(node);
        //return bfs(node);
    }
    
private:
    unordered_map<UndirectedGraphNode*, UndirectedGraphNode*> D;// D: original graph node -> new graph node
    
};