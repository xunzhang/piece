// https://oj.leetcode.com/problems/distinct-subsequences/

class Solution {
public:
    int numDistinct(string S, string T) {
        vector<vector<int> > table(S.size() + 1, vector<int>(T.size() + 1, 0));
        for(int i = 0; i <= T.size(); ++i) table[0][i] = 0;
        for(int i = 0; i <= S.size(); ++i) table[i][0] = 1;
        for(int i = 1; i <= S.size(); ++i) {
            for(int j = 1; j <= T.size(); ++j) {
                if(S[i-1] == T[j-1]) {
                    table[i][j] = table[i-1][j] + table[i-1][j-1];
                } else {
                    table[i][j] = table[i-1][j];
                }
            }
        }
        return table[S.size()][T.size()];
    }
};