// https://oj.leetcode.com/problems/one-edit-distance/

class Solution {
public:
    bool isOneEditDistance(string s, string t) {
        int m = s.size(), n = t.size();
        if(m < n) return isOneEditDistance(t, s);
        if(m - n > 1) return false;
        if(m == n) {
            int cnt = 0;
            for(int i = 0; i < m; ++i) { if(s[i] != t[i]) { cnt ++; } }
            return cnt == 1;
        } else {
            // m - n = 1
            bool flag = false;
            int i = 0, j = 0;
            for(; i < m && j < n; ++i, ++j) {
                if(s[i] != t[j]) {
                    if(flag) return false;
                    i ++; flag = true;
                    if(s[i] != t[j]) return false;
                }
            }
        }
        return true;
    }
};