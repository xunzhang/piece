// https://oj.leetcode.com/problems/permutation-sequence/

class Solution {
public:
    
    string getPermutation(int n, int k) {
        string result;
        string val;
        vector<int> fac(n);
        fac[0] = 1;
        for(int i = 1; i <= n; ++i) val.push_back('0' + i);
        for(int i = 1; i < n; ++i) fac[i] = fac[i-1] * i;
        for(int i = n - 1; i > 0; --i) {
            int indx = (k - 1) / fac[i];
            result.push_back(val[indx]);
            val.erase(val.begin() + indx);
            k -= indx * fac[i];
            
        }
        result.push_back(val[0]);
        return result;
    }

};