// https://oj.leetcode.com/problems/multiply-strings/

class Solution {
public:

    string multiply(string num1, string num2) {
        if(num1 == "0" || num2 == "0") return "0";
        string result(num1.size() + num2.size(), '0');
        for(int j = num2.size() - 1; j >= 0; --j) {
            int carry = 0;
            int N = num2[j] - '0';
            for(int i = num1.size() - 1; i >= 0; --i) {
                int tmp = (num1[i] - '0') * N + carry + (result[i+j+1] - '0');
                carry = tmp / 10;
                result[i+j+1] = tmp % 10 + '0';
            }
            result[j] = carry + '0';
        }
        if(result[0] == '0') return result.substr(1, result.size() - 1);
        return result;
    }

};