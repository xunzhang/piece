// https://oj.leetcode.com/problems/binary-search-tree-iterator/

/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class BSTIterator {
    
public:
    BSTIterator(TreeNode *root) {
        if(root) {
            stk.push(root);
            while(root->left) {
                stk.push(root->left);
                root = root->left;
            }
        }
    }

    /** @return whether we have a next smallest number */
    bool hasNext() {
        return !stk.empty();
    }

    /** @return the next smallest number */
    int next() {
        TreeNode *v = stk.top();
        int r = v->val;
        stk.pop();
        if(v->right) {
            stk.push(v->right);
            TreeNode *tmp = v->right;
            while(tmp->left) {
                stk.push(tmp->left);
                tmp = tmp->left;
            }
        }
        return r;
    }

private:
    stack<TreeNode *> stk;
};

/**
 * Your BSTIterator will be called like this:
 * BSTIterator i = BSTIterator(root);
 * while (i.hasNext()) cout << i.next();
 */