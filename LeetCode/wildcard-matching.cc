// https://oj.leetcode.com/problems/wildcard-matching/

class Solution {
public:
    
    bool isMatch(const char *s, const char *p) {
	    const char *mark = NULL;
	    const char *star = NULL;
	    while(*s) {
	        if(*p == *s || *p == '?') { s++; p++; continue; }
	        if(*p == '*') { star = p; mark = s; p++; continue; }
		if(star) { mark += 1; s = mark; p = star + 1; continue; }
		return false;
	    }
	    while(*p && *p == '*') p++;
	    return *p == '\0';
    }

};