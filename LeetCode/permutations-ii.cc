// https://oj.leetcode.com/problems/permutations-ii/

class Solution {
public:

    void helper(vector<int> & num, vector<int> & visited, vector<int> & tmp, vector<vector<int> > & result) {
        if(tmp.size() == num.size()) {
            result.push_back(tmp);
            return;
        }
        for(int i = 0; i < num.size(); ++i) {
            if(visited[i] == 0) {
                if(i > 0 && num[i] == num[i-1] && visited[i-1] == 1) continue;
                // if(i > 0 && num[i] == num[i-1] && visited[i-1] == 0) continue;
                visited[i] = 1;
                tmp.push_back(num[i]);
                helper(num, visited, tmp, result);
                tmp.pop_back();
                visited[i] = 0;
            }
        }
    }
    
    vector<vector<int> > permuteUnique(vector<int> &num) {
        vector<vector<int> > r;
        vector<int> tmp;
        vector<int> visited(num.size(), 0);
        sort(num.begin(), num.end());
        helper(num, visited, tmp, r);
        return r;
    }
};