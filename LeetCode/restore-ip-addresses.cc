// https://oj.leetcode.com/problems/restore-ip-addresses/

class Solution {
public:

    bool is_validate(string & s) {
        if(s.size() < 1 || s.size() > 3) return false;
        if(s.size() > 1 && s[0] == '0') return false;
        int v = std::stoi(s);
        if(v >= 0 && v <= 255) return true;
        return false;
    }

    void helper(string & input, int len, string & tmp, vector<string> & result) {
        if(len == 1) {
            if(is_validate(input)) result.push_back(tmp + input);
            return;
        }
        for(int i = 1; i <= 3 && i <= input.size(); ++i) {
            string ss = input.substr(0, i);
            if(is_validate(ss)) {
                tmp += ss + ".";
                string new_input = input.substr(i, input.size() - i);
                helper(new_input, len - 1, tmp, result);
                tmp.erase(tmp.size() - (ss.size() + 1), ss.size() + 1);
             }
        }
    }

    vector<string> restoreIpAddresses(string s) {
        vector<string> r;
        string tmp;
        helper(s, 4, tmp, r);
        return r;
    }

};