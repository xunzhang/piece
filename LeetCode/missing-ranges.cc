// https://oj.leetcode.com/problems/missing-ranges/

class Solution {
public:
    
    vector<string> findMissingRanges(int A[], int n, int lower, int upper) {
        vector<string> result;
        int prev = lower - 1;
        for(int i = 0; i <= n; ++i) {
            int curr = i == n ? upper + 1: A[i];
            if(curr - prev >= 2) result.push_back(fmt(prev + 1, curr - 1));
            prev = curr;
        }
        return result;
    }
    
    string fmt(int start, int end) {
        if(start == end) return to_string(start);
        return to_string(start) + "->" + to_string(end);
    }
};