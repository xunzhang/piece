// https://oj.leetcode.com/problems/reverse-nodes-in-k-group/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *reverseKGroup(ListNode *head, int k) {
        if(k <= 1) return head;
        ListNode **p = &head;
        while(*p) {
            ListNode *q = *p;
            // check enough
            for(int i = 0; i < k; ++i) {
                if(q == NULL) return head;
                q = q->next;
            }
            // reverse k node
            q = *p;
            ListNode *prev = NULL;
            for(int i = 0; i < k; ++i) {
                ListNode *tmp = q->next;
                q->next = prev;
                prev = q;
                q = tmp;
            }
            (*p)->next = q;
            *p = prev;
            for(int i = 0; i < k; ++i) {
                p = &((*p)->next); // 这里不能影响head
            }
        }
        return head;
    }
};