// https://oj.leetcode.com/problems/integer-to-roman/

class Solution {
public:
    string intToRoman(int num) {
        string r;
        int i = 0;
        while(num) {
            int result = num / val[i];
            for(int k = 0; k < result; ++k) { r += sym[i]; }
            num -= result * val[i];
            i++;
        }
        return r;
    }
private:
    vector<int> val = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
    vector<string> sym = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
};