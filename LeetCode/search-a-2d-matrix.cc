// https://oj.leetcode.com/problems/search-a-2d-matrix/

class Solution {
public:
/*
    bool searchMatrix(vector<vector<int> > &matrix, int target) {
        int i;
        for(i = 0; i < matrix.size(); ++i) {
            if(matrix[i].back() >= target) { break; }
        }
        if(i == matrix.size()) return false;
        int start = 0, end = matrix[i].size() - 1;
        while(start <= end) {
            int mid = start + (end - start) / 2;
            if(matrix[i][mid] == target) {
                return true;
            } else if(matrix[i][mid] < target) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }
        return false;
    }
*/
    bool searchMatrix(vector<vector<int> > &matrix, int target) {
        int i;
        int s = 0, e = matrix.size() - 1;
        int m = 0;
        while(s <= e) {
            m = s + (e - s) / 2;
            if(target < matrix[m].back()) {
                if(e == m) break;
                e = m;
            } else if(target > matrix[m].back()) {
                s = m + 1;
            } else {
                return true;
            }
        }
        
        s = 0, e = matrix[0].size() - 1;
        while(s <= e) {
            int mid = s + (e - s) / 2;
            if(matrix[m][mid] == target) {
                return true;
            } else if(matrix[m][mid] < target) {
                s = mid + 1;
            } else {
                e = mid - 1;
            }
        }
        return false;
    }
};