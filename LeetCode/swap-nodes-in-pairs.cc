// https://oj.leetcode.com/problems/swap-nodes-in-pairs/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
/*
    ListNode *swapPairs(ListNode *head) {
        if(head == NULL || head->next == NULL) return head;
        ListNode **p = &head;
        while(*p && (*p)->next) {
            ListNode *tmp = (*p)->next;
            (*p)->next = tmp->next;
            tmp->next = *p;
            *p = tmp;
            p = &(*p)->next->next;
        }
        return head;
    }
*/
    
    ListNode *swapPairs(ListNode *head) {
        if(!head || !head->next) return head;
        ListNode *tmp = head->next;
        head->next = swapPairs(tmp->next);
        tmp->next = head;
        return tmp;
    }

};