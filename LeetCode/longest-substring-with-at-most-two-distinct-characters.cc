// https://oj.leetcode.com/problems/longest-substring-with-at-most-two-distinct-characters/

class Solution {
public:

    /*
    // i记录sliding window的起点, j记录前一个字符最后一次出现的位置, 用于替换i, 注意情况aabbaaabbbaaa
    int lengthOfLongestSubstringTwoDistinct(string s) {
        int i = 0, j = -1;
        int maxlen = INT_MIN;
        for(int k = 1; k < s.size(); ++k) {
            if(s[k] == s[k-1]) continue;
            if(j >= 0 && s[k] != s[j]) {
                maxlen = max(maxlen, k - i);
                i = j + 1;
            }
            j = k - 1;
        }
        return max((int)s.size() - i, maxlen);
    }
    */ 
    
    // follow up
    int foo(string s, int KD) {
        int i = 0;
        int D = 0;
        int maxlen = 0;
        vector<int> count(256, 0);
        for(int j = 0; j < s.size(); ++j) {
            if(count[s[j]] == 0) { D++; }
            count[s[j]] ++;
            while(D > KD) {
                count[s[i]]--;
                if(count[s[i]] == 0) D--;
                i++;
            }
            maxlen = max(j-i+1, maxlen);
        }
        return maxlen;
    }
    
    int lengthOfLongestSubstringTwoDistinct(string s) {
        return foo(s, 2);
    }

};