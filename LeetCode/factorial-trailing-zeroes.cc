// https://oj.leetcode.com/problems/factorial-trailing-zeroes/

class Solution {
public:
    int trailingZeroes(int n) {
        int r = 0;
        while(n) {
            r += n / 5;
            n /= 5;
        }
        return r;
    }
};