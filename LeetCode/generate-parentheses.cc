// https://oj.leetcode.com/problems/generate-parentheses/
// 个数是卡特兰数

class Solution {
public:

    // lnum: 左括号数，rnum: 右括号数
     void foo(int lnum, int rnum, int n, string & tmp, vector<string> & result) {
       if(lnum < rnum) return;
       if(lnum == n) {
           while(rnum < n) {
               tmp += ')';
               rnum ++;
           }
           result.push_back(tmp);
           return;
       }
       if(lnum == rnum) {
           string new_tmp = tmp + "(";
           foo(lnum + 1, rnum, n, new_tmp, result);
       } else {
           // l > r
           string new_tmp1 = tmp + "(";
           foo(lnum + 1, rnum, n, new_tmp1, result);
           string new_tmp2 = tmp + ")";
           foo(lnum, rnum + 1, n, new_tmp2, result);
       }
   }
   
    vector<string> generateParenthesis(int n) {
        vector<string> result;
        string tmp;
        foo(0, 0, n, tmp, result);
        return result;
    }
};