// https://oj.leetcode.com/problems/first-missing-positive/

class Solution {
public:
    // bitmap A[i] = i+1 if exist else 0 or negative
    int firstMissingPositive(int A[], int n) {
        int i;
        for(i = 0; i < n; ++i) {
            if(A[i] <= 0 || A[i] == i + 1) continue;
            while(!(A[i] <= 0 || A[i] == i + 1)) {
                int indx = A[i] - 1;
                if(indx >= n) { A[i] = 0; break; }
                if(A[indx] == indx + 1) { A[i] = 0; break; } // 换的目标已经就绪，这个不能行就只能返回
                swap(A[i], A[indx]);
            }
        }
        for(i = 0; i < n; ++i) {
            if(A[i] <= 0) return i + 1;
        }
        return i + 1;
    }
};