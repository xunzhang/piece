// https://oj.leetcode.com/problems/surrounded-regions/

class Solution {
public:

    void solve(vector<vector<char>> &board) {
        if(board.size() == 0) return;
        int m = board.size();
        int n = board[0].size();
        vector<vector<int> > alive(m, vector<int>(n, 0));
        queue<pair<int, int> > Q;
        for(int i = 0; i < m; ++i) {
            if(board[i][0] == 'O') {
                alive[i][0] = 1; 
                Q.push(make_pair(i, 0));
            }
            if(board[i][n-1] == 'O') {
                alive[i][n-1] = 1;
                Q.push(make_pair(i, n-1));
            }
        }
        
        for(int i = 0; i < n; ++i) {
            if(board[0][i] == 'O') {
                alive[0][i] = 1;
                Q.push(make_pair(0, i));

            }
            if(board[m-1][i] == 'O') {
                alive[m-1][i] = 1;
                Q.push(make_pair(m-1, i));
            }
        }
        
        // 注意其中alive[][] == 0可以避免死循环
        while(!Q.empty()) {
            auto pr = Q.front(); Q.pop();
            int i = pr.first, j = pr.second;
            if(i-1 > 0 && board[i-1][j] == 'O' && alive[i-1][j] == 0) {
                alive[i-1][j] = 1;
                Q.push(make_pair(i-1, j));
            }            
            if(i+1 < m - 1 && board[i+1][j] == 'O' && alive[i+1][j] == 0) {
                alive[i+1][j] = 1;
                Q.push(make_pair(i+1, j));
            }
            if(j-1 > 0 && board[i][j-1] == 'O' && alive[i][j-1] == 0) {
                alive[i][j-1] = 1;
                Q.push(make_pair(i, j-1));
            }
            if(j + 1 < n - 1 && board[i][j+1] == 'O' && alive[i][j+1] == 0) {
                alive[i][j+1] = 1;
                Q.push(make_pair(i, j+1));
            }
        }
        
        for(int i = 0; i < m; ++i) {
            for(int j = 0; j < n; ++j) {
                if(alive[i][j] == 0 && board[i][j] == 'O') {
                    board[i][j] = 'X';
                }
            }
        }
    }
};