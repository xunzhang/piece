// https://oj.leetcode.com/problems/combinations/

class Solution {
public:

    void foo(int start, int n, int k, vector<int> & path, vector<vector<int> > & result) {
        if(k == 0) {
            result.push_back(path);
            return;
        }
        for(int i = start; i <= n - k + 1; ++i) {
            path.push_back(i);
            foo(i + 1, n, k - 1, path, result);
            path.pop_back();
        }
    }
   
    vector<vector<int> > combine(int n, int k) {
        vector<int> path;
        vector<vector<int> > result;
        foo(1, n, k, path, result);
        return result;
    }
    
};