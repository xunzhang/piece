// https://oj.leetcode.com/problems/sqrtx/

class Solution {
public:
    int sqrt(int x) {
        double v = (double)x;
        double s = 0., e = v, m = v / 2;
        if(x == 1) { return 1; }
        while(fabs(m * m - v) > 0.01) {
            m = s + (e - s) / 2;
            if(m * m >= v) {
                e = m;
            } else {
                s = m;
            }
        }
        return (int)m;
    }
    
    /* Newton method
    int sqrt(int x) {
        int r = 0;
        double i = 1.;
        int it = 100;
        while(it > 0) {
            i = (i + (double)x / i) / 2;
            it -= 1;
        }
        return (int)i;
    }
    */
};