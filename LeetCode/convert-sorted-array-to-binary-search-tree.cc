// https://oj.leetcode.com/problems/convert-sorted-array-to-binary-search-tree/

/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
 
    TreeNode *sortedArrayToBST(vector<int> &num) {
        if(num.size() == 0) return NULL;
        int indx = num.size() / 2;
        TreeNode *p = new TreeNode(num[indx]);
        vector<int> a(num.begin(), num.begin() + indx);
        vector<int> b(num.begin() + indx + 1, num.end());
        p->left = sortedArrayToBST(a);
        p->right = sortedArrayToBST(b);
        return p;
    }
};