// https://oj.leetcode.com/problems/reverse-linked-list-ii/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:

    /*
    ListNode *reverseBetween(ListNode *head, int m, int n) {
        if(m == n) { return head; }
        ListNode *it1 = head, *it2 = head, *piece_head = NULL;
        for(int i = 0; i < m - 1; ++i) {
            piece_head = it1;
            it1 = it1->next;
        }
        for(int i = 0; i < n - 1; ++i) {
            it2 = it2->next;
        }
        ListNode *p = it1;
        ListNode *c = it1->next;
        ListNode *piece_tail = it2->next;
        while(c != piece_tail) {
            ListNode *nn = c->next;
            c->next= p;
            p = c;
            c = nn;
        }
        if(piece_head == NULL) {
            head = it2;
        } else {
            piece_head->next = it2;
        }
        it1->next = c;
        return head;
    }
    */

    ListNode *reverseBetween(ListNode *head, int m, int n) {
        if(m == n) return head;
        ListNode *prev = NULL, *curr = head;
        
        for(int i = 1; i < m; ++i) {
            prev = curr;
            curr = curr->next;
        }
        
        ListNode *tmp_prev = NULL, *tmp_curr = curr;
        for(int i = 0; i <= n - m; ++i) {
            ListNode *tmp = curr->next;
            curr->next = tmp_prev;
            tmp_prev = curr;
            curr = tmp;
        }
        
        if(prev) {
            prev->next = tmp_prev;
        } else {
            // m = 1
            head = tmp_prev;
        }
        tmp_curr->next = curr;
        return head;
    }
    
};