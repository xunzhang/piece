// https://oj.leetcode.com/problems/search-insert-position/

class Solution {
public:
    int searchInsert(int A[], int n, int target) {
        int s = 0, e = n - 1;
        int m;
        while(s <= e) {
            m = s + (e - s) / 2;
            if(A[m] < target) {
                s = m + 1;
            } else if(A[m] > target) {
                e = m - 1;
            } else {
                return m;
            }
        }
        return A[m] < target ? m + 1 : m;
    }
};