// https://oj.leetcode.com/problems/longest-consecutive-sequence/

class Solution {
public:
    int longestConsecutive(vector<int> &num) {
        for(int i = 0; i < num.size(); ++i) S.insert(num[i]);
        int result = INT_MIN;
        for(auto & v : num) {
            int cnt = 1;
            int l = v - 1, r = v + 1;
            while(S.count(l)) {
                cnt += 1;
                S.erase(l);
                l -= 1;
            }
            while(S.count(r)) {
                cnt += 1;
                S.erase(r);
                r += 1;
            }
            S.erase(v);
            result = max(result, cnt);
        }
        return result;
    }
private:
    unordered_set<int> S;
};