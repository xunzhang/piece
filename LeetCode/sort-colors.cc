// https://oj.leetcode.com/problems/sort-colors/

class Solution {
public:
    void sortColors(int A[], int n) {
        int p_1 = 0, p_2 = n - 1;
        for(int i = 0; i < n;) {
            if(A[i] == 0) {
                swap(A[i], A[p_1]);
                p_1 += 1; i += 1;
                continue;
            }
            if(A[i] == 1) {
                i += 1; continue;
            }
            if(A[i] == 2) {
                swap(A[i], A[p_2]);
                p_2 -= 1; n -= 1;
                continue;
            }
        }
    }
};