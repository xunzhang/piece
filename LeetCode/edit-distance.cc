// https://oj.leetcode.com/problems/edit-distance/

class Solution {
public:
    int minDistance(string word1, string word2) {
        if(word1 == word2) { return 0; }
        int m = word1.size(), n = word2.size();
        if(m == 0) return n;
        if(n == 0) return m;
        int f[m][n]; f[0][0] = 1;
        if(word1[0] == word2[0]) f[0][0] = 0;
        bool flag = false;
        for(int i = 1; i < m; ++i) {
            f[i][0] = f[i-1][0] + 1;
            if(word1[i] == word2[0] && flag == false) { 
                f[i][0] -= 1;
                flag = true;
            }
        }
        flag = false;
        for(int i = 1; i < n; ++i) {
            f[0][i] = f[0][i-1] + 1;
            if(word2[i] == word1[0] && flag == false) {
                f[0][i] -= 1;
                flag = true;
            }
        }
        for(int i = 1; i < m; ++i) {
            for(int j = 1; j < n; ++j) {
                int a = f[i-1][j] + 1;
                int b = f[i][j-1] + 1;
                int curry = word1[i] == word2[j] ? 0 : 1;
                int c = f[i-1][j-1] + curry;
                f[i][j] = min(min(a, b), c);
            }
        }
        return f[m-1][n-1];
    }
};