// https://oj.leetcode.com/problems/search-in-rotated-sorted-array-ii/

class Solution {
public:

    bool search(int A[], int n, int target) {
        int s = 0, e = n - 1;
        while(s <= e) {
            int m = s + (e - s) / 2;
            if(A[m] == target) { return true; }
            if(A[s] < A[m]) {
                if(target < A[m] && target >= A[s]) {
                    e = m - 1;
                } else {
                    s = m + 1;
                }
            } else if(A[s] > A[m]) {
                if(target > A[m] && target <= A[e]) {
                    s = m + 1;
                } else {
                    e = m - 1;
                }
            } else {
                s = s + 1;
            }
        }
        return false;
    }

/*
    bool search(int A[], int n, int target) {
        for(int i = 0; i < n; ++i) {
            if(A[i] == target) return true;
        }
        return false;
    }
*/

};