// https://oj.leetcode.com/problems/path-sum-ii/

/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<vector<int> > pathSum(TreeNode *root, int sum) {
        if(root == NULL) return result;
        foo(root, sum);
        return result;
    }
    
    void foo(TreeNode *p, int v) {
        tmp.push_back(p->val);
        if(p->left == NULL && p->right == NULL) {
            if(p->val == v) {
                result.push_back(tmp);
            }
        } else {
            if(p->left) {
                foo(p->left, v - p->val);
            }
            if(p->right) {
                foo(p->right, v - p->val);
            }
        }
        tmp.pop_back();
    }
    
private:
    vector<int> tmp;
    vector<vector<int> > result;
};