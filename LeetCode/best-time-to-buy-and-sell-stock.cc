// https://oj.leetcode.com/problems/best-time-to-buy-and-sell-stock/

class Solution {
public:
    int maxProfit(vector<int> &prices) {
        if(prices.size() == 0 || prices.size() == 1) { return 0; }
        int mmin = INT_MAX;
        int max_profit = 0;
        for(int i = 0; i < prices.size(); ++i) {
            max_profit = max(max_profit, prices[i] - mmin);
            mmin = min(prices[i], mmin);
        }
        return max_profit;
    }
};