// https://oj.leetcode.com/problems/palindrome-number/

class Solution {
public:
    bool isPalindrome(int x) {
        if(x < 0) return false;
        if(x < 10) return true;
        int div = 10;
        while(x / div >= 10) div *= 10;
        while(1) {
            int l = x / div;
            int r = x % 10;
            if(l != r) return false;
            x = (x % div) / 10;
            div /= 100;
            if(x < 10 && div <= 10) break;
        }
        return true;
    }
};