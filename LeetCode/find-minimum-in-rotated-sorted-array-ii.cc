// https://oj.leetcode.com/problems/find-minimum-in-rotated-sorted-array-ii/

class Solution {
public:
    int findMin(vector<int> &num) {
        int s = 0, e = num.size() - 1;
        int m;
        while(s <= e) {
            m = s + (e - s) / 2;
            if(num[m] > num[e]) {
                s = m + 1;
            } else if(num[m] < num[e]){
                e = m;
            } else {
                // O(n)
                if(num[m] == num[s]) s++;
                if(num[m] == num[e]) e--;
            }
        }
        return num[m];
    }
};