// https://oj.leetcode.com/problems/longest-palindromic-substring/

class Solution {
public:

    // O(n*n)
    /*
    string longestPalindrome(string input) {
        int start = 0;
        int maxlen = 1;
        bool table[1000][1000];
        for(int i = 0; i < input.size(); ++i) { table[i][i] = true; }
        for(int i = 0; i < input.size() - 1; ++i) {
            if(input[i] == input[i+1]) {
                table[i][i+1] = true;
                maxlen = 2;
                start = i;
            } else {
                table[i][i+1] = false;
            }   
        }
        for(int len = 3; len <= input.size(); ++len) {
            for(int i = 0; i < input.size() - len + 1; ++i) {
                int j = i + len - 1;
                if(input[i] == input[j] && table[i+1][j-1]) {
                    table[i][j] = true;
                    maxlen = len;
                    start = i;
                } else {
                    table[i][j] = false;
                }   
            }   
        }
        return input.substr(start, maxlen);
    }
    */
    
    // O(n)
    string expand(string & input, int s, int e) {
        while(input[s] == input[e] && s >= 0 && e < input.size()) {
            s--;
            e++;
        }
        return input.substr(s + 1, e - s - 1); 
    }

    string longestPalindrome(string input) {
        if(input.size() == 0) { return ""; }
        string R = input.substr(0, 1);
        for(int i = 0; i < input.size() - 1; ++i) {
            string tmp1 = expand(input, i, i); // camac
            if(tmp1.size() > R.size()) {
                R = tmp1;
            }
            string tmp2 = expand(input, i, i + 1); // abba
            if(tmp2.size() > R.size()) {
                R = tmp2;
            }
        }
        return R;
    }

};