// https://oj.leetcode.com/problems/longest-valid-parentheses/

class Solution {
public:

    int longestValidParentheses2(string s) {
        stack<int> stk;
        int r = 0;
        for(int i = 0; i < s.size(); ++i) {
            if(s[i] == '(') { 
                stk.push(i);
            } else {
                if(stk.empty()) { stk.push(i); continue; }
                if(s[stk.top()] == '(') {
                    stk.pop();
                    if(stk.empty()) {
                        r = i + 1;
                    } else {
                        r = max(i - stk.top(), r);
                    }
                } else {
                    stk.push(i);
                }
            }
        }
        return r;
    }
    
    // https://leetcodenotes.wordpress.com/2013/10/19/leetcode-longest-valid-parentheses-%E8%BF%99%E7%A7%8D%E6%8B%AC%E5%8F%B7%E7%BB%84%E5%90%88%EF%BC%8C%E6%9C%80%E9%95%BF%E7%9A%84valid%E6%8B%AC%E5%8F%B7%E7%BB%84%E5%90%88%E6%9C%89%E5%A4%9A/
    //  DP解法 T[i]表示从s[i]到s.end()的合法substring
    int longestValidParentheses(string s) {
        if(s.size() < 2) return 0;
        vector<int> T(s.size(), 0);
        int maxlen = INT_MIN;
        for(int i = s.size() - 2; i >= 0; --i) {
            if(s[i] == ')') {
                T[i] = 0;
            } else {
                int indx = i + 1 + T[i+1];
                if(indx < s.size() && s[indx] == ')') {
                    T[i] = T[i+1] + 2;
                    if(indx + 1 < s.size()) T[i] += T[indx+1];
                }
            }
            maxlen = max(maxlen, T[i]);
        }
        return maxlen;
    }
};
