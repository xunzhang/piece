// https://oj.leetcode.com/problems/reorder-list/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* reverse(ListNode *h) {
        if(h == NULL || h->next == NULL) { return h; }
        ListNode *prev = NULL, *curr = h;
        while(curr) {
            ListNode *tmp = curr->next;
            curr->next = prev;
            prev = curr;
            curr = tmp;
        }
        return prev;
    }
    
    void reorderList(ListNode *head) {
        ListNode *l1 = head, *l2 = head;
        if(head == NULL || head->next == NULL) { return; }
        while(l2->next && l2->next->next) {
            l1 = l1->next;
            l2 = l2->next->next;
        }
        ListNode *p1 = head;
        ListNode *p2 = l1->next;
        l1->next = NULL;
        
        p2 = reverse(p2);
        
        // merge
        while(p1 && p2) {
            ListNode *tmp = p1->next;
            ListNode *tmp2 = p2->next;
            p1->next = p2;
            p2->next = tmp;
            p1 = tmp;
            p2 = tmp2;
        }
    }
};