/**
 * Definition for a point.
 * struct Point {
 *     int x;
 *     int y;
 *     Point() : x(0), y(0) {}
 *     Point(int a, int b) : x(a), y(b) {}
 * };
 */
class Solution {
public:
    
    int maxPoints(vector<Point> &points) {
        int result = 0;
        for(int i = 0; i < points.size(); ++i) {
            unordered_map<double, int> D;
            D[INT_MIN] = 0; // for dup
            int dup = 1;
            for(int j = 0; j < points.size(); ++j) {
                if(j == i) continue;
                if(points[i].x == points[j].x && points[i].y == points[j].y) { dup ++; continue; }
                double k = points[i].x == points[j].x ? INT_MAX : static_cast<double>(points[i].y - points[j].y) / static_cast<double>(points[i].x - points[j].x);
                D[k] += 1;
            }
            for(auto & kv : D) result = max(kv.second + dup, result);
        }
        return result;
    }

};