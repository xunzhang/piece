// https://oj.leetcode.com/problems/powx-n/

class Solution {
public:
/*
    // recursive
    double pow(double x, int n) {
        if(n == 0) { return 1; }
        int exp = abs(n);
        double half = pow(x, exp >> 1);
        double r = half * half;
        if(exp & 1) { r *= x; }
        return n > 0 ? r : 1 / r;
    }
*/
    // iterative
    double pow(double x, int n) {
        //if(n == 0) { return 1; }
        double r = 1;
        int exp = abs(n);
        while(exp > 0) {
            if(exp & 1) { r *= x; }
            x *= x;
            exp = exp >> 1;
        }
        return n > 0 ? r : 1 / r;
    }
    
};