// https://oj.leetcode.com/problems/reverse-words-in-a-string-ii/

class Solution {
public:
    void reverseWords(string &s) {
        for(int i = 0; i < s.size();) {
            int j = i + 1;
            while(j < s.size() && s[j] != ' ') j ++;
            int ss = i, ee = j - 1;
            while(ss < ee) {
                swap(s[ss], s[ee]);
                ss ++; ee --;
            }
            i = j + 1;
        }
        int ss = 0, ee = s.size() - 1;
        while(ss < ee) {
            swap(s[ss], s[ee]);
            ss ++; ee --;
        }
    }
};