// https://oj.leetcode.com/problems/anagrams/

class Solution {
public:

    /*
    vector<string> anagrams(vector<string> &strs) {
        vector<string> r;
        map<string, int> dct;
        for(int i = 0; i < strs.size(); ++i) {
            string key = strs[i];
            sort(key.begin(), key.end());
            if(!dct.count(key)) {
                dct[key] = i;
            } else {
                if(dct[key] != -1) {
                    r.push_back(strs[dct[key]]);
                }
                r.push_back(strs[i]);
                dct[key] = -1;
            }
        }
        return r;
    }
    */
    
    vector<string> anagrams(vector<string> &strs) {
        vector<string> result;
        unordered_map<string, int> D;
        for(int i = 0; i < strs.size(); ++i) {
            string word = strs[i];
            sort(word.begin(), word.end());
            if(D.count(word)) {
                if(D[word] != -1) { result.push_back(strs[D[word]]); D[word] = -1; }
                result.push_back(strs[i]);
            } else {
                D[word] = i;
            }
        }
        return result;
    }
    
};