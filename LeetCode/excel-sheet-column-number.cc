// https://oj.leetcode.com/problems/excel-sheet-column-number/

class Solution {
public:
    int titleToNumber(string s) {
        int r = 0;
        for(int i = 0; i < s.size(); ++i) {
            r += (s[i] - 'A' + 1) * pow(26, s.size() - i - 1);
        }
        return r;
    }
};