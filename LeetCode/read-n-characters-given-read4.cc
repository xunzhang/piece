// https://oj.leetcode.com/problems/read-n-characters-given-read4/

// Forward declaration of the read4 API.
int read4(char *buf);

class Solution {
public:

    /**
     * @param buf Destination buffer
     * @param n   Maximum number of characters to read
     * @return    The number of characters read
     */
    int read(char *buf, int n) {
        char tmp[4];
        bool eof = false;
        int read_bytes = 0;
        while(!eof && read_bytes < n) {
	    int nbytes = read4(tmp);
            if(nbytes < 4) eof = true;
            int sz = min(nbytes, n - read_bytes);
            memcpy(buf + read_bytes, tmp, sz);
	        read_bytes += sz;
        }
        return read_bytes;
    }

};