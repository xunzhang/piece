// https://oj.leetcode.com/problems/pascals-triangle-ii/

class Solution {
public:
    /*
     *      0  1  2  3  4  5  6
     * 0th  1
     * 1th  1  1
     * 2th  1  2  1
     * 3th  1  3  3  1
     * 4th  1  4  6  4  1
     * 5th  1  5 10 10  5  1
     * 6th  1  6 15 20 15  6  1
     */
    vector<int> getRow(int rowIndex) {
        vector<int> row(rowIndex + 1, 0);
        row[0] = 1;
        for(int r = 1; r <= rowIndex; ++r) {
            for(int j = r; j > 0; --j) {
                row[j] += row[j-1];
            }
        }
        return row;
    }
};