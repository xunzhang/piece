// https://oj.leetcode.com/problems/triangle/

class Solution {
public:
    
    /*
    // 按位控制, 注意indx累加, O(n * 2^n) time, O(1) space
    int minimumTotal(vector<vector<int> > &triangle) {
        int r = triangle[0][0];
        int sz = triangle.size() - 1;
        int min_sum = 0;
        for(int i = 0; i < triangle.size(); ++i) {
            min_sum += triangle[i][0];
        }
        for(int i = 0; i < pow(2, sz); ++i) {
            int indx = 0;
            int sum = r;
            for(int level = 0; level < sz; ++level) {
                indx += ((i >> (sz - level-1)) & 1);
                sum += triangle[level + 1][indx];
            }
            min_sum = min(sum, min_sum);
        }
        return min_sum;
    }
    */

    /*
    int minimumTotal(vector<vector<int> > &triangle) {
        return foo(triangle, 0, 0);
    }
    
    // c++坑: 注意min(a, b)不一定先跑a还是b
    int foo(vector<vector<int> > &triangle, int row, int column) {
        int tmp = triangle[row][column];
        if(row == triangle.size() - 1) return tmp;
        int aa = min_sum_table.count(row + 1) ? min_sum_table[row+1] : foo(triangle, row + 1, column);
        int bb = foo(triangle, row + 1, column + 1);
        tmp += min(aa, bb);
        min_sum_table[row] = tmp;
        return tmp;
    }
    */
    
    int minimumTotal(vector<vector<int> > & triangle) {
        int result = 10000;
        for(int i = 0; i < triangle.back().size(); ++i) min_sum_table[i] = triangle.back()[i];
        for(int row = triangle.size() - 2; row >= 0; --row) {
            for(int column = 0; column < triangle[row].size(); ++column) {
                min_sum_table[column] = min(triangle[row][column] + min_sum_table[column],
                                  triangle[row][column] + min_sum_table[column+1]);
            }   
        }
        return min_sum_table[0];
    }

    
private:
    unordered_map<int, int> min_sum_table;
};