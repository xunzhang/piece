// https://oj.leetcode.com/problems/zigzag-conversion/

class Solution {
public:

    string convert(string s, int nRows) {
        if(nRows == 1) { return s; }
        if(s.size() <= nRows) { return s;}
        string r;
        int interval = 2 * (nRows - 1);
        int row = 0;
        for(int k = nRows - 1; k >= 0; --k) {
            int dist = 2 * k;
            int indx = row;
            while(indx < s.size()) {
                r.push_back(s[indx]);
                if(dist == 0) { dist = interval - dist; }
                indx += dist;
                dist = interval - dist;
            }
            row += 1;
        }
        return r;
    }

};