// https://oj.leetcode.com/problems/remove-duplicates-from-sorted-list/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    
    ListNode *deleteDuplicates(ListNode *head) {
        ListNode *p1 = head;
        while(p1) {
            ListNode *p2 = p1->next;
            if(!p2) break;
            while(p2->val == p1->val) {
                ListNode *tmp = p2->next;
                delete p2;
                p2 = tmp;
                if(!p2) break;
            }
            p1->next = p2;
            p1 = p1->next;
        }
        return head;
    }
};