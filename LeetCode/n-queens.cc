// https://oj.leetcode.com/problems/n-queens/

class Solution {
public:
    // 检查row行和之前的所有行是否满足即可
    bool valid(int row, vector<int> & column) {
        for(int i = 0; i < row; ++i) {
            if( (column[row] == column[i]) || ((row - i) == abs(column[row] - column[i])) ) return false;
        }
        return true;
    }

    void helper(int n, int row, vector<int> & column, vector<vector<string> > & result) {
        if(row == n) {
            vector<string> tmp;
            for(int i = 0; i < n; ++i) {
                string ttmp(n, '.'); // 注意这里构造函数和vector有别
                ttmp[column[i]] = 'Q';
                tmp.push_back(ttmp);
            }
            result.push_back(tmp);
            return;
        }
        for(int i = 0; i < n; ++i) {
            column[row] = i;
            if(valid(row, column)) {
                helper(n, row + 1, column, result);
            }
            column[row] = -1;
        }
    }

    vector<vector<string> > solveNQueens(int n) {
        vector<int> column(n, -1);
        vector<vector<string> > result;
        helper(n, 0, column, result);
        return result;
    }
};