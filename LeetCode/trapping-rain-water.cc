// https://oj.leetcode.com/problems/trapping-rain-water/

class Solution {
public:

    // 对于每一个bar往两边扫描，找到它能承受的最大水量
    int trap(int A[], int n) {
        int *lmost = new int [n];
        int tmp_max = A[0];
        for(int i = 1; i < n; ++i) {
            lmost[i] = tmp_max;
            if(A[i] > tmp_max) { tmp_max = A[i]; }
        }
        int water = 0;
        tmp_max = A[n-1];
        for(int i = n - 2; i > 0; --i) {
            int lbnd = lmost[i];
            int rbnd = tmp_max;
            if(min(lbnd, rbnd) > A[i]) { water += min(lbnd, rbnd) - A[i]; }
            if(A[i] > tmp_max) { tmp_max = A[i]; }
        }
        delete [] lmost;
        return water;
    }
};