// https://oj.leetcode.com/problems/next-permutation/

class Solution {
public:
    void nextPermutation(vector<int> &num) {
        int i = num.size() - 2;
        while(i >= 0 && num[i] >= num[i+1]) i --; // 注意等号
        if(i == -1) {
            reverse(num.begin(), num.end());
            return;
        }
        for(int k = num.size() - 1; k > i; --k) {
            if(num[k] > num[i]) {
                swap(num[k], num[i]);
                break;
            }
        }
        reverse(num.begin() + i + 1, num.end());
    }
};