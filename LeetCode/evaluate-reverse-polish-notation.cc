// https://oj.leetcode.com/problems/evaluate-reverse-polish-notation/

class Solution {
public:
    int evalRPN(vector<string> &tokens) {
        for(int i = 0; i < tokens.size(); ++i) {
            if(tokens[i] == "+") {
                int right = stk.top();
                stk.pop();
                int left = stk.top();
                stk.pop();
                stk.push(left + right);
            } else if (tokens[i] == "-") {
                int right = stk.top();
                stk.pop();
                int left = stk.top();
                stk.pop();
                stk.push(left - right);
            } else if (tokens[i] == "*") {
                int right = stk.top();
                stk.pop();
                int left = stk.top();
                stk.pop();
                stk.push(left * right);
            } else if(tokens[i] == "/") {
                int right = stk.top();
                stk.pop();
                int left = stk.top();
                stk.pop();
                stk.push(left / right);
            } else {
                stk.push(stoi(tokens[i]));
            }
        }
        return stk.top();
    }
private:
    stack<int> stk;
};