// https://oj.leetcode.com/problems/simplify-path/

class Solution {
public:
    string simplifyPath(string path) {
        string r = "/";
        int i = 1;
        while(i < path.size()) {
            string ss; // 存两个/之间的东西
            while(path[i] != '/' && i < path.size()) {
                ss.push_back(path[i]);
                i += 1;
            }
            if(ss.size() == 0) { i++; continue; }
            if(ss == "..") {
                if(!stk.empty()) stk.pop();
            } else if(ss == ".") {
		i += 1;
                continue;
            } else {
                stk.push(ss);
            }
            i += 1;
        } // main while
        vector<string> temp;
        while(!stk.empty()) {
            temp.push_back(stk.top());
            stk.pop();
        }
        for(int k = temp.size() - 1; k >= 0; --k) {
            r += temp[k] + "/";
        }
        if(r != "/") {
            r.pop_back();
        }
        return r;
    }
private:
    stack<string> stk;
};