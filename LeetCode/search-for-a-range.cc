// https://oj.leetcode.com/problems/search-for-a-range/

class Solution {
public:
    vector<int> searchRange(int A[], int n, int target) {
        vector<int> result;
        int s = 0, e = n - 1;
        while(s <= e) {
            int m = s + (e-s) / 2;
            if(A[m] == target) {
                int left = m, right = m;
                while(left - 1 >= 0 && A[m] == A[left-1]) { left -= 1; }
                while(right + 1 < n && A[m] == A[right+1]) { right += 1; }
                result.push_back(left); result.push_back(right);
                return result;
            } else if(A[m] < target) {
                s = m + 1;
            } else {
                e = m - 1;
            }
        }
        result.push_back(-1); result.push_back(-1);
        return result;
    }
};