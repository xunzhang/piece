// https://oj.leetcode.com/problems/reverse-words-in-a-string/

class Solution {
public:

    /*
    void reverseWords(string &s) {
        if(s.size() == 0) { return; }
        string local = s;
        s.resize(0);
        int end = local.size() - 1;
        while(local[end] == ' ' && end >= 0) {
            end -= 1;
        }
        for(int i = end; i >= 0;) {
            string word;
            while(i >= 0 && local[i] != ' ') {
                word.push_back(local[i]);
                i -= 1;
            }
            for(int j = word.size() - 1; j >= 0; --j) {
                s.push_back(word[j]);
            }
            while(i >= 0 && local[i] == ' ') {
                i -= 1;
            }
            s.push_back(' ');
        }
        if(s[s.size()-1] == ' ') {
          s.pop_back();
        }
    }
    */

    void reverseWords(string &s) {
        if(s.size() == 0) return;
        string tmp = s; int i = s.size() - 1;
        s.resize(0);
        while(i >= 0) {
            while(tmp[i] == ' ' && i >= 0) --i;
            if(i < 0) break;
            int j = i - 1;
            while(tmp[j] != ' ' && j >= 0) --j;
            for(int k = j + 1; k <= i; ++k) s.push_back(tmp[k]);
            s.push_back(' ');
            i = j - 1;
        }
        if(s.back() == ' ') s.pop_back();
    }

};