// https://oj.leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/

class Solution {
public:
    int maxProfit(vector<int> &prices) {
        if(prices.size() == 0 || prices.size() == 1) { return 0; }
        int max_profit = 0;
        for(int i = 0; i < prices.size() - 1; ++i) {
            int delta = prices[i+1] - prices[i];
            if(delta > 0) {
                max_profit += delta;
            }
        }
        return max_profit;
    }
};