// https://oj.leetcode.com/problems/binary-tree-maximum-path-sum/

/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int maxPathSum(TreeNode *root) {
        foo(root);
        return max_sum;
    }
    int foo(TreeNode *p) {
        if(p == NULL) return 0;
        int lmax = foo(p->left);
        int rmax = foo(p->right);
        max_sum = max(p->val + lmax + rmax, max_sum); // 是否终结没个点都有可能, 所以每都要和max_sum比一次, 4种可能都在里面了
        int ret = p->val + max(lmax, rmax); // 如果不是终结, 则返回给父结点只能是一边
        return max(ret, 0);
    }
private:
    int max_sum = INT_MIN;
};