// https://oj.leetcode.com/problems/subsets/

class Solution {
public:

    vector<int> foo(int x, vector<int> &S) {
        vector<int> r;
        for(int k = 0; k < S.size(); ++k) {
            if(x % 2 == 1) { r.push_back(S[k]); }
            x /= 2;
        }
        return r;
    }

    vector<vector<int> > subsets(vector<int> &S) {
        vector<vector<int> > r;
        sort(S.begin(), S.end());
        for(int i = 0; i < (1 << S.size()); ++i) {
            vector<int> s = foo(i, S);
            r.push_back(s);
        }
        return r;
    }

/*
    vector<vector<int> > subsets(vector<int> & S) {
        sort(S.begin(), S.end());
        vector<vector<int> > r;
        vector<int> ntmp;
        r.push_back(ntmp);
        for(int i = S.size() - 1; i >= 0; --i) {
            int sz = r.size();
            for(int j = 0; j < sz; ++j) {
                vector<int> buf;
                buf.push_back(S[i]);
                buf.insert(buf.end(), r[j].begin(), r[j].end());
                r.push_back(buf);
            }   
        }
        return r;
    }
*/
};