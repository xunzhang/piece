// https://oj.leetcode.com/problems/subsets-ii/

class Solution {
public:

    vector<int> foo(int x, vector<int> &S) {
        vector<int> r;
        for(int k = 0; k < S.size(); ++k) {
            if(x % 2 == 1) { r.push_back(S[k]); }
            x /= 2;
        }
        return r;
    }

    vector<vector<int> > subsetsWithDup(vector<int> &S) {
        set<vector<int> > r;
        vector<vector<int> > result;
        sort(S.begin(), S.end());
        for(int i = 0; i < (1 << S.size()); ++i) {
            vector<int> s = foo(i, S);
            r.insert(s);
        }
        for(auto & item : r) {
            result.push_back(item);
        }
        return result;
    }

/*
    vector<vector<int> > subsetsWithDup(vector<int> & S) {
        sort(S.begin(), S.end());
        vector<vector<int> > r;
        vector<int> ntmp;
        r.push_back(ntmp);
        if(S.size() == 0) { return r; }
        vector<int> nntmp;
        nntmp.push_back(S[0]);
        r.push_back(nntmp);
        int cnt = 0;
        for(int i = 1; i < S.size(); ++i) {
            bool flag; 
            if(S[i] == S[i-1]) {
                cnt ++; flag = true;
            } else {
                cnt = 0; flag = false;
            }   
            int sz = r.size();
            for(int j = 0; j < sz; ++j) {
                vector<int> buf(r[j].begin(), r[j].end());
                if(flag && r[j].size() < cnt) { continue; }
                if(flag && r[j][r[j].size()-cnt] != S[i]) { continue; }
                buf.push_back(S[i]);
                r.push_back(buf);
            }
        }
        return r;
    }
*/

};