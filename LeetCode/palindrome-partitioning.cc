// https://oj.leetcode.com/problems/palindrome-partitioning/

class Solution {
public:
    bool is_palindrome(string & str) {
        int s = 0, e = str.size() - 1;
        while(s < e) {
            if(str[s] != str[e]) { return false; }
            s ++;
            e --;
        }
        return true;
    }
    
    void helper(string & s, int start_indx, vector<string> & tmp, vector<vector<string>> & r) {
        if(start_indx == s.size()) {
            r.push_back(tmp);
            return;
        }
        for(int i = start_indx; i < s.size(); ++i) {
            string ss = s.substr(start_indx, i - start_indx + 1);
            if(is_palindrome(ss)) {
                tmp.push_back(ss);
                helper(s, i + 1, tmp, r);
                tmp.pop_back();
            }
        }
    }
    
    vector<vector<string>> partition(string s) {
        vector<vector<string>> r;
        vector<string> tmp;
        helper(s, 0, tmp, r);
        return r;
    }
};