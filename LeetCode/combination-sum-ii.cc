// https://oj.leetcode.com/problems/combination-sum-ii/

class Solution {
public:
        
    void foo(vector<int> &candidates, int target, int sum, int start, vector<int> & tmp, vector<vector<int> > & result) {
        if(sum >= target) {
            if(sum == target) result.push_back(tmp);
            return;
        }
        for(int i = start; i < candidates.size();) {
            tmp.push_back(candidates[i]);
            foo(candidates, target, sum + candidates[i], i+1, tmp, result);
            tmp.pop_back();
            int j = i + 1;
            while(j < candidates.size() && candidates[j] == candidates[i]) j++;
            i = j;
        }
    }
    
    vector<vector<int> > combinationSum2(vector<int> &candidates, int target) {
        vector<int> tmp;
        vector<vector<int> > result;
        sort(candidates.begin(), candidates.end());
        foo(candidates, target, 0, 0, tmp, result);
        return result;
    }
    
};