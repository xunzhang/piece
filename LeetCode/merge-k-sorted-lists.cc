// https://oj.leetcode.com/problems/merge-k-sorted-lists/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
  
    // O(nklogk)
    ListNode *mergeKLists(vector<ListNode *> &lists) {
        if(lists.size() == 0) return NULL;
        while(lists.size() > 1) {
            int s = 0, e = lists.size() - 1;
            while(s < e) {
                lists[s] = mergeTwoLists(lists[s], lists[e]);
                lists.pop_back();
                s++; e--;
            }
        }
        return lists[0];
    }
    
    ListNode *mergeTwoLists(ListNode *l1, ListNode *l2) {
        ListNode *lm;
        if(l1 == NULL && l2 == NULL) { return NULL; }
        if(l1 == NULL) { return l2; }
        if(l2 == NULL) { return l1; }
        if(l1->val < l2->val) {
            lm = l1; l1 = l1->next;
        } else {
            lm = l2; l2 = l2->next;
        }
        ListNode *tmp = lm;
        while(l1 && l2) {
            if(l1->val < l2->val) {
                tmp->next = l1;
                l1 = l1->next;
            } else {
                tmp->next = l2;
                l2 = l2->next;
            }
            tmp = tmp->next;
        }
        if(l1) { tmp->next = l1; }
        if(l2) { tmp->next = l2; }
        return lm;
    }
};