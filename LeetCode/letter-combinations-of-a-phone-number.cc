// https://oj.leetcode.com/problems/letter-combinations-of-a-phone-number/

class Solution {
public:
    Solution() {
        tbl['#'] = ""; 
        tbl['*'] = ""; 
        tbl['0'] = ""; 
        tbl['1'] = ""; 
        tbl['2'] = "abc";
        tbl['3'] = "def";
        tbl['4'] = "ghi";
        tbl['5'] = "jkl";
        tbl['6'] = "mno";
        tbl['7'] = "pqrs";
        tbl['8'] = "tuv";
        tbl['9'] = "wxyz";
    }
    
    void helper(string & input, string & tmp, vector<string> & result) {
        if(input.size() == 0) {
            result.push_back(tmp);
            return;
        }
        if(tbl[input[0]].size() == 0) {
            string new_input = input.substr(1, input.size() - 1); 
            helper(new_input, tmp, result);
        }
        for(auto & letter : tbl[input[0]]) {
            tmp.push_back(letter);
            string new_input = input.substr(1, input.size() - 1); 
            helper(new_input, tmp, result);
            tmp.pop_back();
        }
    }

    vector<string> letterCombinations(string digits) {
        vector<string> r;
        string tmp;
        helper(digits, tmp, r); 
        return r;
    }

private:
    map<char, string> tbl;

};