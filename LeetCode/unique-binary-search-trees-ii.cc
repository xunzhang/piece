// https://oj.leetcode.com/problems/unique-binary-search-trees-ii/

/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<TreeNode *> generateTrees(int n) {
        if(n == 0) {
            vector<TreeNode *> r;
            r.push_back(NULL);
            return r;
        }
        
        // TreesTable[i][l]表示 最小结点i 最大结点为i+l 共l+1个结点 的树 的集合
        // TreesTable[i]是vector<vector>, 它表示最小结点为i的树的集合: [[最小结点i, 最大结点为i集合1], [最小结点为i, 最大结点i+1集合2], ...]
        vector<vector<vector<TreeNode *> > > TreesTable(n);
        for(int i = 0; i < n; ++i) {
            TreesTable[i].resize(n-i);
        }

        // 初始化一个结点的树集
        for(int i = 0; i < n; ++i) {
            TreeNode *node = new TreeNode(i+1);
            TreesTable[i][0].push_back(node);
        }
        
        for(int offset = 1; offset < n; ++offset) {
            for(int start = 0; start + offset <= n - 1; ++start) {
                // 这里要构造一个起点是start, 终点是start+offset的树集
                for(int k = start; k <= start + offset; ++k) {
                    //这里要构造一个根为k, 左子树是start ... k-1, 右子树为k+1...start+offset的树集
                    if(k == start) {
                        for(auto & right_node : TreesTable[k+1][offset-1]) {
                            TreeNode *node = new TreeNode(k+1);
                            node->right = right_node;
                            TreesTable[start][offset].push_back(node);
                        }
                    } else if(k == start + offset) {
                        for(auto & left_node : TreesTable[start][offset-1]) {
                            TreeNode *node = new TreeNode(k+1);
                            node->left = left_node;
                            TreesTable[start][offset].push_back(node);
                        }
                    } else {
                        for(auto & left_node : TreesTable[start][k-1-start]) {
                            for(auto & right_node : TreesTable[k+1][start+offset-k-1]) {
                                TreeNode *node = new TreeNode(k+1);
                                node->left = left_node;
                                node->right = right_node;
                                TreesTable[start][offset].push_back(node);
                            }
                        }
                    }
                }
            }    
        }
        
        return TreesTable[0][n-1];
    }
};