// https://oj.leetcode.com/problems/interleaving-string/

class Solution {
public:

    //  table[i][j]表示是否: S1(0, i)和S2(0,j)，匹配S3(0,i+j)
    bool isInterleave(string s1, string s2, string s3) {
        if(s1.size() + s2.size() != s3.size()) return false;
        if(s1.empty() && s2.empty() && s3.empty()) return true;
        int n1 = s1.size(), n2 = s2.size();
        vector<vector<int> > table(n1 + 1, vector<int>(n2 + 1, 0));
        table[0][0] = 1;
        for(int i = 1; i <= n1; ++i) {
            if(s3[i-1] == s1[i-1]) table[i][0] = table[i-1][0];
        }
        for(int j = 1; j <= n2; ++j) {
            if(s3[j-1] == s2[j-1]) table[0][j] = table[0][j-1];
        }
        for(int i = 1; i <= n1; ++i) {
            for(int j = 1; j <= n2; ++j) {
                table[i][j] = (table[i-1][j] && s3[i+j-1] == s1[i-1]) || (table[i][j-1] && s3[i+j-1] == s2[j-1]);
            }
        }
        return table[n1][n2];
    }
};