// https://oj.leetcode.com/problems/implement-strstr/

class Solution {
public:
    /*
    int strStr(char *haystack, char *needle) {
        string a(haystack), b(needle);
        for(int i = 0; ; ++i) {
            for(int j = 0; ; ++j) {
                if(j == b.size()) return i;
                if(i + j >= a.size()) return -1;
                if(a[i+j] != b[j]) break;
            }
        }
    }
    */
    
    int strStr(char *haystack, char *needle) {
        for(int i = 0; haystack[i] != '\0'; ++i) {
            for(int j = 0; ; ++j) {
                if(needle[j] == '\0') return i;
                if(haystack[i+j] == '\0') return -1;
                if(haystack[i+j] != needle[j]) break;
            }
        }
        if(*needle == '\0') return 0;
        return -1;
    }
    
};