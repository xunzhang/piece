// https://oj.leetcode.com/problems/search-in-rotated-sorted-array/

class Solution {
public:
    
    int search(int A[], int n, int target) {
        int s = 0, e = n -1;
        while(s <= e) {
            int m = s + (e-s) / 2;
            if(A[m] == target) return m;
            if(A[m] >= A[s]) {
                if(target < A[m] && target >= A[s]) {
                    e = m - 1;
                } else {
                    s = m + 1;
                }
            } else {
                if(target > A[m] && target <= A[e]) {
                    s = m + 1;
                } else {
                    e = m - 1;
                }
            }
        }
        return -1;
    }
};