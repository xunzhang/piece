// https://oj.leetcode.com/problems/majority-element/

class Solution {
public:
    int majorityElement(vector<int> &num) {
        int r = num[0], cnt = 1;
        for(int i = 1; i < num.size(); ++i) {
            if(num[i] == r) {
                cnt += 1;
            } else {
                cnt -= 1;
                if(cnt == 0) { r = num[i+1]; }
            }
        }
        return r;
    }
};