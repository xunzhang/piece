// https://oj.leetcode.com/problems/remove-duplicates-from-sorted-array/

class Solution {
public:
    
    int removeDuplicates(int A[], int n) {
        if(n <= 1) return n;
        int j = 1;
        for(int i = 1; i < n; ++i) {
            if(A[i] != A[j-1]) {
                A[j] = A[i];
                j++;
            }
        }
        return j;
    }

};