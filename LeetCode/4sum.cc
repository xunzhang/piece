// https://oj.leetcode.com/problems/4sum/

class Solution {
public:
    vector<vector<int> > fourSum(vector<int> &num, int target) {
        vector<vector<int> > result;
        if(num.size() <=3) return result;
        sort(num.begin(), num.end());
        for(int i = 0; i < num.size() - 3; ++i) {
            if(i > 0 && num[i] == num[i-1]) continue;
            for(int j = i + 1; j < num.size() - 2; ++j) {
                if(j > i + 1 && num[j] == num[j-1]) continue;
                int p1 = j + 1, p2 = num.size() - 1;
                while(p1 < p2) {
                    int sum = num[i] + num[j] + num[p1] + num[p2];
                    if(sum < target) {
                        p1 += 1;
                    } else if(sum > target) {
                        p2 -= 1;
                    } else {
                        result.push_back(vector<int>({num[i], num[j], num[p1], num[p2]}));
                        p1 += 1; p2 -= 1;
                        while(p1 < p2 && num[p1] == num[p1-1]) p1 += 1;
                        while(p1 < p2 && num[p2] == num[p2+1]) p2 -= 1;
                    }
                }
            }
        }
        return result;
    }
};