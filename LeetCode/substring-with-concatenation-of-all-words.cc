// https://oj.leetcode.com/problems/substring-with-concatenation-of-all-words/

class Solution {
public:

    /*
    vector<int> findSubstring(string S, vector<string> &L) {
        vector<int> result;
        int l = L[0].size();
        unordered_map<string, int> D, tmp;
        for(int i = 0; i < L.size(); ++i) D[L[i]] += 1;
        int i = 0;
        for(; i < S.size(); ++i) {
            string ss = S.substr(i, l);
            if(D.count(ss)) {
                tmp = D;
                int j = i, cnt = 0;
                while(tmp[ss] > 0) {
                    tmp[ss] -= 1;
                    i += l;
                    cnt += 1;
                    if(i >= S.size()) break;
                    ss = S.substr(i, l);
                }
                if(cnt == L.size()) result.push_back(j);
                i = j;
            }
            if(i + L.size() * l >= S.size()) break;
        }
        return result;
    }
    */
    
    vector<int> findSubstring(string S, vector<string> &L) {
        vector<int> result;
        unordered_map<string, int> D;
        for(int i = 0; i < L.size(); ++i) D[L[i]] += 1;
        int l = L[0].size();
        for(int i = 0; i < S.size(); ++i) {
            unordered_map<string, int> tmp = D;
            string ss = S.substr(i, l);
            int cnt = 0, j = i;
            while(tmp[ss] > 0) {
                cnt += 1;
                tmp[ss] -= 1;
                i += l;
                if(i > S.size()) break;
                ss = S.substr(i, l);
            }
            i = j;
            if(cnt == L.size()) result.push_back(j);
            if(i + L.size() * l > S.size()) break;
        }
        return result;
    }
};