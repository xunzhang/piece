// https://oj.leetcode.com/problems/binary-tree-upside-down/

/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:

    // Top down
    TreeNode *upsideDownBinaryTree2(TreeNode *root) {
        TreeNode *p = root, *parent = NULL, *parentRight = NULL;
        while(p) {
            TreeNode *left = p->left;
            p->left = parentRight;
            parentRight = p->right;
            p->right = parent;
            parent = p;
            p = left;
        }
        return parent;
    }
    
    // Bottom up
    TreeNode *upsideDownBinaryTree(TreeNode *root) {
        return dfs(root, NULL);
    }
    TreeNode *dfs(TreeNode *p, TreeNode *parent) {
        if(!p) return parent;
        TreeNode *root = dfs(p->left, p); // 最左变成newroot
        p->left = parent ? parent->right : NULL;
        p->right = parent;
        return root;
    }

};