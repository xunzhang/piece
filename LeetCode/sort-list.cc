// https://oj.leetcode.com/problems/sort-list/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    
    ListNode *sortList(ListNode *head) {
        if(head == NULL || head->next == NULL) {
            return head;
        }
        ListNode *l1 = head, *l2 = head;
        while(l2->next && l2->next->next) {
            l1 = l1->next;
            l2 = l2->next->next;
        }
        ListNode *h2 = l1->next;
        l1->next = NULL;
        ListNode *h1 = head;
        h1 = sortList(h1);
        h2 = sortList(h2);
        return merge(h1, h2);
    }
    
    ListNode *merge(ListNode *l1, ListNode *l2) {
        ListNode *lm;
        if(l1 == NULL && l2 == NULL) { return NULL; }
        if(l1 == NULL) { return l2; }
        if(l2 == NULL) { return l1; }
        if(l1->val < l2->val) {
            lm = l1; l1 = l1->next;
        } else {
            lm = l2; l2 = l2->next;
        }
        ListNode *tmp = lm;
        while(l1 && l2) {
            if(l1->val < l2->val) {
                tmp->next = l1;
                l1 = l1->next;
            } else {
                tmp->next = l2;
                l2 = l2->next;
            }
            tmp = tmp->next;
        }
        if(l1) { tmp->next = l1; }
        if(l2) { tmp->next = l2; }
        return lm;
    }
    
};