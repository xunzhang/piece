// https://oj.leetcode.com/problems/insertion-sort-list/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:

    ListNode *insertionSortList(ListNode *head) {
        if(!head || !head->next) return head;
        ListNode *newbegin = head, *p = head->next;
        newbegin->next = NULL;
        while(p) {
            ListNode *curr = p;
            p = p->next;
            ListNode *it = newbegin;
            while(it->next) {
                if(it->next->val >= curr->val) break;
                it = it->next;
            }
            if(it == newbegin && it->val > curr->val) {
                newbegin = curr;
                curr->next = it;
            } else {
                ListNode *tmp = it->next;
                it->next = curr;
                curr->next = tmp;
            }
        }
        return newbegin;
    }

};