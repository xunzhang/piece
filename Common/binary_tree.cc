#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

struct node {
  int val;
  struct node *left, *right;
  node(int v) : val(v), left(NULL), right(NULL) {} 
};

class binary_tree {

 private:
  node* init(int *VLR, int *LVR, int n) {
    if(n <= 0) return NULL;
    int k = 0;
    for(; VLR[0] != LVR[k]; ++k);
    node *t = new node(VLR[0]);
    t->left = init(VLR + 1, LVR, k);
    t->right = init(VLR + 1 + k, LVR + 1 + k, n - k - 1);
    return t;
  }

  void destroy(node *p) {
    auto free_lambda = [&] (node *t) {
      delete t;
    };
    post_order(p, free_lambda);
  }

 public:
  binary_tree(vector<int> & VLR, vector<int> & LVR) {
    root = init(&VLR[0], &LVR[0], VLR.size());
  }
  
  ~binary_tree() {
    destroy(root);
  }
  
  template <class F>
  void pre_order(node *p, F & func) {
    if(p == NULL) return;
    func(p);
    pre_order(p->left, func);
    pre_order(p->right, func);
  }
  
  template <class F>
  void in_order(node *p, F & func) {
    if(p == NULL) return;
    in_order(p->left, func);
    func(p);
    in_order(p->right, func);
  }
   
  template <class F>
  void post_order(node *p, F & func) {
    if(p == NULL) return;
    post_order(p->left, func);
    post_order(p->right, func);
    func(p);
  }
   
  int count(node *p) {
    if(p == NULL) return 0;
    return count(p->left) + count(p->right) + 1;
  }
  
  int depth(node *p) {
    if(p == NULL) return 0;
    return max(depth(p->left), depth(p->right)) + 1;
  }

  node* get_root() { return root; }

 private:
   node *root = NULL;
};

class bst {
 public:
  node *search(node *p, int key) {
    if(p == NULL) return NULL;
    if(p->val > key) return search(p->left, key);
    if(p->val < key) return search(p->right, key);
    return p;
  }

  node *insert(node *p, int key) {
    if(p == NULL) {
      node *new_node = new node(key);
      return new_node;
    }
    if(p->val > key) {
      p->left = insert(p->left, key); 
    } else {
      p->right = insert(p->right, key); 
    }
    return p;
  }

  node *del(node *p, int key) {
  }

 private:
  node *root = NULL;
};

int main(int argc, char *argv[])
{
  {
    vector<int> pre_seq = {4, 2, 1, 3, 6, 5, 7}, in_seq = {1, 2, 3, 4, 5, 6, 7};
    binary_tree obj(pre_seq, in_seq);
    node *p = obj.get_root();
    auto print_lambda = [&] (node *p) {
      cout << p->val << endl;
    };
    obj.pre_order(p, print_lambda);
    cout << "---" << endl;
    obj.in_order(p, print_lambda);
    cout << "---" << endl;
    obj.post_order(p, print_lambda);
    cout << "---" << endl;
  }
  return 0;
}
