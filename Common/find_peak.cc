#include <vector>
#include <iostream>
using namespace std;

int foo(vector<int> & data) {
  int s = 0, e = data.size() - 1;
  while(s <= e) {
    if(s == e) return data[s];
    int m = s + (e - s) / 2;
    if(data[m] < data[m+1]) s = m + 1;
    if(data[m] > data[m+1]) e = m;
  }
  return -1;
}

int main(int argc, char *argv[])
{
  vector<int> data1 = {1, 2, 3, 4, 2, 1};
  vector<int> data2 = {1, 2, 3};
  vector<int> data3 = {1, 2, 3, 2, 1};
  vector<int> data4 = {1, 2};
  vector<int> data5 = {2};
  vector<int> data6 = {2, 1};
  vector<int> data7 = {3, 2, 1};
  cout << foo(data1) << endl; // 4
  cout << foo(data2) << endl; // 3
  cout << foo(data3) << endl; // 3
  cout << foo(data4) << endl; // 2
  cout << foo(data5) << endl; // 2
  cout << foo(data6) << endl; // 2
  cout << foo(data7) << endl; // 3
  return 0;
}
