// a1, a2, a3, a4, ... ai-1, ai, ai+1, ... an
// ai is balance element if a1+a2+...+ai-1 = ai+1+...+an

#include <vector>
#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
  vector<int> data2 = {1, 2, 3, 1, 1, 1};
  vector<int> data = {2, 1, -1};
  vector<int> result;
  int S = 0, E = 0;
  for(int i = 0; i < data.size(); ++i) E += data[i];
  for(int i = 0; i < data.size(); ++i) {
    if(i > 0) S += data[i-1];
    E -= data[i];
    if(S == E) result.push_back(data[i]);
  }
  for(auto & v : result) cout << v << endl;
  return 0;
}
