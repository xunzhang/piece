// http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=116469&extra=page%3D1%26filter%3Dsortid%26sortid%3D311%26sortid%3D311

// 1. 给你一个set 和一个数字S 然后让你 从set里面找到三个数字之和小于等于S 求一共有几种可能
// 2. 你有一堆 job 每个job有起始和终止时间，用户，以及需要的内存。问你给你一个用户，求他在所有时间内所占用最多的内存是多少？比如他有两个job重叠各占了1G 那你就要说是2G了 
// 3. 一个matrix 要完成update(x,y,value) 和 sum(x1,y1,x2,y2) 两个function 即 改一个值和求一个长方形区域内的总和.
// 4. 一个a*b的matrix 一共N个格子 里面含有 1..N 各一个 让你找出最长的连续增长Path
// 5. 假设有N个球[1..N] 以及N+1个盒子[1..N+1] 然后让你把所有东西归位 即第N个盒子里放第N个求 最后一个盒子就当你的buffer。每一个action必须是把球从A拿到B，swap就算三个Action了（从A拿到空盒子，把B拿到A，把空盒子拿到B）

// 第一题: foo
// 第二题: 参考../Leetcode/merge-intervals.cc, O(n)
// 第三题: http://www.java3z.com/cwbwebhome/article/article1/1369.html?id=4804

#include <vector>
#include <string>
#include <algorithm>
#include <tuple>
#include <iostream>
using namespace std;

vector<tuple<int, int, int> > foo(vector<int> & data, int target) {
  vector<tuple<int, int, int> > result;
  sort(data.begin(), data.end());
  for(int i = 0; i < data.size() - 2; ++i) {
    int p1 = i + 1, p2 = data.size() - 1;
    while(p1 < p2) {
      int sum = data[i] + data[p1] + data[p2];
      if(sum > target) p2 --;
      if(sum < target) { 
        result.push_back(make_tuple(data[i], data[p1], data[p2])); 
        p1 ++;
      }
      if(sum == target) {
        result.push_back(make_tuple(data[i], data[p1], data[p2])); 
        p2 --;
      }
    }
  }
  return result;
}

int main(int argc, char *argv[])
{
  vector<int> data = {1, 2, 3, 4, 5, 6, 7};
  auto r = foo(data, 10);
  for(auto & v : r) {
    std::cout << std::get<0>(v) << "|" << std::get<1>(v) << "|" <<
        std::get<2>(v) << std::endl;
  }
  return 0;
}
